CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

Chat NodeJS is a Drupal module of the Chat. As a backend used asynchronous 
non-blocking NodeJS technology. Therefore, chat is completely interactive and 
any change in its user interface is immediately visible in the browser.

Module Chat NodeJS can provide an excellent way of communication for the 
registered users of your WEB-site. Users are able to contact each other and/or 
WEB-site administration privately in a real time mode. Also Chat NodeJS could 
serve as a useful component of the typical blogs, thematiс forums and social 
networks. Module Chat NodeJS is a great opportunity for the online commerce 
WEB-resources to provide support of their products and/or services thus 
increasing effectiveness of any online business.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/chat_nodejs

 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/chat_nodejs


REQUIREMENTS
------------

This module requires "Node.js integration" Drupal module for the functioning.


INSTALLATION
------------

 * Install the Node.js integration module and setup accordingly to the
   instructions.
  
 * Copy file chat_nodejs.server.extension.js from 
   "chat_nodejs/js/chat_nodejs.server" to the 
   "node_modules/drupal-node.js/extensions" folder.
  
 * Configure node.config.js in the "node_modules/drupal-node.js/" folder
   to load the chat_nodejs.server.extension.
 
 * Install the Chat NodeJS module as you would normally install
   a contributed Drupal module.
   Visit https://www.drupal.org/node/1897420 for further information.
   
 * Run app.js in the "node_modules/drupal-node.js" folder.  
 

CONFIGURATION
-------------

    1. Navigate to Administration > Extend and enable the module.
    2. Navigate to Administration » People » Permissions:
        *  access chat nodejs
          "Access Chat NodeJS UI."
    3. Navigate to Administration > Configuration > Web services >
       Configure Chat NodeJS menu to setup a basic module configuration.
        * choose the default notification sound state
        * adjust the chat visibility


MAINTAINERS
-----------

 * Yurii Slan (Engineer_UA) - https://www.drupal.org/u/engineer_ua
