<?php

/**
 * @file
 * Contains Drupal\chat_nodejs\Form\chatSettingsForm
 */

namespace Drupal\chat_nodejs\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class chatSettingsForm
 *
 * @package Drupal\chat_nodejs\Form
 */
class chatSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'chat_nodejs.settings',
    ];
  }

  /**
  * {@inheritdoc}
  */
  public function getFormId() {
    return 'settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm( array $form, FormStateInterface $form_state ) {

    $config = $this->config('chat_nodejs.settings');

    $form['#attached']['library'][] = 'chat_nodejs/chat-nodejs-path-visibility';

    $form['chat_general_settings'] = array(
      '#type' => 'details',
      '#title' => $this->t('General Settings'),
      '#open' => TRUE,
    );

    $form['chat_general_settings']['chat_notification_sound'] = array(
      '#type' => 'select',
      '#title' => $this->t('Notification Sound'),
      '#description' => $this->t('Enable/disable notification sound when a new message is received/sent.'),
      '#options' => array(1 => 'Yes', 2 => 'No'),
      '#default_value' => $config->get('chat_notification_sound') ?: 1,
    );

    $form['chat_path'] = array(
      '#type' => 'details',
      '#title' => $this->t('Chat Visibility'),
      '#open' => TRUE
    );

    $options = array(
      0 => $this->t('Everywhere'),
      1 => $this->t('All pages except those listed'),
      2 => $this->t('Only the listed pages'),
      3 => $this->t('Disable')
    );
    $description = $this->t("Specify pages by using their paths. Enter one path per line. The '*' character is a wildcard. Example paths are %blog for the blog page and %blog-wildcard for every personal blog. %front is the front page.", array('%blog' => 'blog', '%blog-wildcard' => 'blog/*', '%front' => '<front>'));
    $title = $this->t('Pages');

    $form['chat_path']['chat_path_visibility'] = array(
      '#type' => 'radios',
      '#title' => $this->t('Show chat on specific pages'),
      '#options' => $options,
      '#default_value' => $config->get('chat_path_visibility') ?: 0,
    );

    $form['chat_path']['chat_path_pages'] = array(
      '#type' => 'textarea',
      '#title' => '<span>' . $title . '</span>',
      '#default_value' => $config->get('chat_path_pages') ?: NULL,
      '#description' => $description,
    );

    return parent::buildForm( $form, $form_state );

  }

  /**
  * {@inheritdoc}
  */
  public function validateForm( array &$form, FormStateInterface $form_state ) {

  }

  /**
  * {@inheritdoc}
  */
  public function submitForm( array &$form, FormStateInterface $form_state ) {

    parent::submitForm( $form, $form_state );

    $this->config( 'chat_nodejs.settings' )
      ->set( 'chat_path_visibility', $form_state->getValue( 'chat_path_visibility' ) )
      ->set( 'chat_path_pages', $form_state->getValue( 'chat_path_pages' ) )
      ->set( 'chat_notification_sound', $form_state->getValue( 'chat_notification_sound' ) )
      ->save();

  }

}
