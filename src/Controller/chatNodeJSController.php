<?php
/**
 * @file
 * Contains Drupal\chat_nodejs\Controller\chatNodeJSController
 */

namespace Drupal\chat_nodejs\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Component\Utility\Html;
use Drupal\user\Entity\User;
use Drupal\file\Entity\File;
use Drupal;

class chatNodeJSController extends ControllerBase {

  public static function chat_verify_access() {

    $user  = Drupal::currentUser();
    // Convert path to lowercase. This allows comparison of the same path
    // with different case. Ex: /Page, /page, /PAGE.
    $pages = mb_strtolower( Drupal::config( 'chat_nodejs.settings' )->get( 'chat_path_pages' ) );
    $chat_path_visibility = Drupal::config( 'chat_nodejs.settings' )->get( 'chat_path_visibility' ) ?: 0;
    $chat_hidden = FALSE;

    // Match path if necessary (if show chat on the specified pages).
    if (Drupal::config('chat_nodejs.settings')->get('chat_path_pages')) {

      if ( $chat_path_visibility == 1 || $chat_path_visibility == 2 ) {

        // Convert the Drupal path to lowercase
        $path = mb_strtolower( Drupal::service( 'path.current' )->getPath() );
        // Compare the lowercase internal and lowercase path alias (if any).
        $path = ltrim( $path, '/' );
        $page_match =  Drupal::service( 'path.matcher' )->matchPath( $path, $pages );

        // When $chat_path_visibility has a value of 1,
        // the block is displayed on all pages except those listed in the text field.
        // When set to 2, it is displayed only on those pages listed in the text field.
        if ( $chat_path_visibility == 1 ) { $page_match = !$page_match; }

      } else {

          $page_match = FALSE;

      }
    // If show chat on the specified pages not used
    } else {

        $page_match = TRUE;

    }

    // If disable chat option has been chosen
    if ($chat_path_visibility == 3) {$chat_hidden = TRUE;}

    $final = ($user->id() >= 0)
      && $page_match
        && !($chat_hidden)
          && $user->hasPermission( 'access chat' );

    return $final;

  }

  /**
  * {@inheritdoc}
  * Send messages in the chat
   * @throws
  */
	public static function chat_send() {

		$account = Drupal::currentUser();
		$token_generator = Drupal::csrfToken();
		$formToken = Html::escape( $_POST[ 'form_token' ] );
		$formID = Html::escape( $_POST[ 'form_id' ] );
		$form_token = !empty( $formToken ) ? $formToken : '';
		$form_id = !empty( $formID ) ? $formID  : '';

		if ( !$token_generator->validate( $form_token, $form_id ) ) {

			return false;

		}

    $message = (object)[];
    $message->message_id = Html::escape( $_POST[ 'chat_message_id' ] );
    $message->uid1 = $account->id();
    $message->uid2 = Html::escape( $_POST[ 'chat_uid2' ] );
    $message->message = $_POST['chat_message'];
    $message->timestamp = time();
    // Photo of the sender to show in the receiver's window
    $message->p = chatNodeJSController::chat_return_pic_url($message->uid1);
    $message->name = $account->getDisplayName();

    Drupal::database()->merge( 'chat_nodejs_msg' )
      ->keys([
        'message_id' => $message->message_id,
        'uid1' => $message->uid1,
        'uid2' => $message->uid2,
      ])
      ->fields([ 'message' => $message->message, 'timestamp' => $message->timestamp ])
      ->execute();

		foreach ( Drupal::moduleHandler()->getImplementations( 'chat_send' ) as $module ) {

			$function = $module . '_chat_send';
			$function( $message );

		}

		return new JsonResponse( "Done!" );

	}

  /**
   * Online buddy list creation
   *
   * @return array
   */
  public static function _chat_buddylist_online() {

    $user = Drupal::currentUser();
    $connection = Drupal::database();
    $users = [];

    $query = $connection
      ->select( 'chat_nodejs_users', 'cu' );
    $query
      ->leftJoin( 'chat_nodejs_relations', 'cr', 'cu.uid = cr.uid1 OR cu.uid = cr.uid2' );
    $query
      ->fields( 'cu', [ 'uid', 'name', 'status', 'session' ] )
      ->condition( 'cu.uid', $user->id(), '<>' )
      ->condition( 'cr.status', 'friends' );
    $condition_1 = $query->andConditionGroup()
      ->condition( 'cr.uid1', $user->id() );
    $condition_2 = $query->andConditionGroup()
      ->condition( 'uid2', $user->id() );
    $condition_general = $query
      ->conditionGroupFactory( $conjunction = 'OR' )
      ->condition( $condition_1 )
      ->condition( $condition_2 );
    $query->condition( $condition_general );

    $user_list = $query
      ->orderBy( 'cu.name' )
      ->execute()->fetchAll();

    $query->condition( 'cu.status', '0', '<>' );
    $users_online = $query->execute()->fetchAll();

    foreach ( $user_list as $buddy ) {

      chatNodeJSController::_chat_buddy( $buddy,$users );

    }

    $users[ 'total' ] = count( $users_online );

    return $users;

  }

  /**
   * Helper function to create one buddy for buddy list
   *
   * @param $buddy
   * @param $users
   * @throws
   */
  public static function _chat_buddy( $buddy, &$users ) {

    $account = Drupal::entityTypeManager()->getStorage( 'user' )->load( $buddy->uid );
    $buddy_name = $account->getDisplayName();
    $users[ $buddy->uid ] = array(
      'name' => ( mb_strlen( $buddy_name ) > 14 )
        ? mb_substr( $buddy_name, 0, 14 ) . '...'
        : $buddy_name,
      'status' => $buddy->status,
      'sender' => property_exists( $buddy, 'sender' ) ? $buddy->sender : 0,
    );

    $users[ $buddy->uid ][ 'p' ] = chatNodeJSController::chat_return_pic_url_any_user( $account );

  }

  /**
   * @throws
   * @return JsonResponse
   */
  public static function chat_get_thread_history() {

    $user = Drupal::currentUser();
    $json = [];
    $connection = Drupal::database();
    $history_length = 30;

    if ( isset( $_POST[ 'chat_open_chat_uids' ] ) ) {

      $today_start = strtotime( 'today' );
      $chat_ids = explode( ',', Html::escape( $_POST[ 'chat_open_chat_uids' ] ) );
      $json[ 'messages' ] = [];
      $json[ 'names' ] = [];
      $page_number = isset( $_POST[ 'history_id' ] ) ? Html::escape( $_POST[ 'history_id' ] ) : 0;

      foreach ( $chat_ids as $chat_id ) {
        // Prepare the names for thread history requests
        $account = Drupal::entityTypeManager()->getStorage( 'user' )->load( $chat_id );

        if ( $account ) {

          $json[ 'names' ][ $chat_id ] = $account->getDisplayName();;

        } else {

          $json[ 'names' ][ $chat_id ] = '';

        }

        $current_uid = $user->id();

          $query = $connection->select( 'chat_nodejs_msg', 'm' )
            ->fields( 'm', [ 'message_id', 'uid1', 'uid2', 'message', 'timestamp' ] );
          $messages_condition_1 = $query->andConditionGroup()
            ->condition( 'm.uid2', $chat_id )
            ->condition( 'm.uid1', $current_uid );
          $messages_condition_2 = $query->andConditionGroup()
            ->condition( 'm.uid2', $current_uid )
            ->condition( 'm.uid1', $chat_id );
          $messages_condition_general = $query
            ->conditionGroupFactory( $conjunction = 'OR' )
            ->condition( $messages_condition_1 )
            ->condition( $messages_condition_2 );
          $query->condition( $messages_condition_general );
          $query->orderBy( 'm.timestamp', 'DESC' )
            ->range( $page_number * $history_length, $history_length - 1 );
          $messages = $query->execute()->fetchAll();

        foreach ( $messages as $message ) {

          if ( $message->timestamp < $today_start ) {

            $date = date( "d/m/y H:i", $message->timestamp );

          } else {

            $date = t("Today ") . date( "H:i", $message->timestamp );

          }

          if ( !strpos( $message->uid1,'-' ) && $message->uid1 != $user->id() ) {

            if ( !strpos( $message->uid1,'-' ) ) {

              $account = User::load( $message->uid1 );
              $temp_msg = array( 'message' => Html::escape( $message->message ), 'timestamp' => $date, 'uid1' => $message->uid1, 'name' => $account->getDisplayName(), 'uid2' => $message->uid2, 'message_id' => Html::escape( $message->message_id ), );
              $temp_msg[ 'p' ] = chatNodeJSController::chat_return_pic_url_any_user( $account );
              $json[ 'messages' ][] = $temp_msg;

            } else {

              $arr = explode( "-", $message->uid1, 2 );
              $sid = $arr[ 1 ];
              $query = $connection->select( 'chat_nodejs_users', 'cu' )
                ->fields( 'cu', [ 'name' ] );
              $query->condition( 'cu.uid', '0' )
                ->condition( 'cu.session', $sid  );
              $name = $query->execute()->fetchField();
              $temp_msg = array( 'message' => Html::escape( $message->message ), 'timestamp' => $date, 'uid1' => $message->uid1, 'name' => $name, 'uid2' => $message->uid2, 'message_id' => Html::escape( $message->message_id ), );
              $temp_msg[ 'p' ] = chatNodeJSController::chat_return_pic_url_any_user( User::load( '0' ) );
              $json[ 'messages' ][] = $temp_msg;

            }

          } else {

            if ( !strpos( $message->uid2,'-' ) ) {

              $account = User::load( $message->uid2 );
              $account1 = User::load( $message->uid1 );
              $temp_msg = array( 'message' => Html::escape( $message->message ), 'timestamp' => $date, 'uid1' => $message->uid1, 'name' => $account->getDisplayName(), 'uid2' => $message->uid2, 'message_id' => Html::escape( $message->message_id ), );
              $temp_msg[ 'p' ] = chatNodeJSController::chat_return_pic_url_any_user( $account1 );
              $json[ 'messages' ][] = $temp_msg;

            } else {

              $arr = explode( "-", $message->uid2, 2 );
              $sid = $arr[ 1 ];
              $query = $connection->select( 'chat_nodejs_users', 'cu' )
                ->fields( 'cu', [ 'name' ] );
              $query->condition( 'cu.uid', '0' )
                ->condition( 'cu.session', $sid  );
              $name = $query->execute()->fetchField();
              $temp_msg = array( 'message' => Html::escape( $message->message ), 'timestamp' => $date, 'uid1' => $message->uid1, 'name' => $name, 'uid2' => $message->uid2, 'message_id' => Html::escape( $message->message_id ), );
              $temp_msg[ 'p' ] = chatNodeJSController::chat_return_pic_url_any_user( User::load( '0' ) );
              $json[ 'messages' ][] = $temp_msg;

            }
          }

        }

      }

      $json[ 'messages' ] = array_reverse( $json[ 'messages' ] );

    }

    return new JsonResponse( $json );

  }

  public static function chat_return_pic_url( $uid = null ) {

      if ( isset( $uid ) ) {

          $u = User::load( $uid );

      } else {

          $u = User::load( Drupal::currentUser()->id() );

      }

      return chatNodeJSController::chat_return_pic_url_any_user( $u );

  }

  /**
   * Get the user account picture url or set default url
   *
   * @param $u
   * @return string
   */
  public static function chat_return_pic_url_any_user( $u ) {

    global $base_url;

    if ( ( !empty( $u->user_picture ) )
      && ( null !== $u->get('user_picture')->entity )) {

      $file_uri = $u->get('user_picture')->entity->getFileUri();
      $url = file_create_url($file_uri);

    // no image set.
    } else {

      $url = $base_url . '/' . drupal_get_path( 'module', 'chat_nodejs' ) . '/css/themes/main/images/default_avatar.png';

    }

    return $url;

  }

  public static function _chat_chat() {

    global $base_url;
    $user = Drupal::currentUser();
    $chat = array();

    $chat[ 'name' ] = 'chat';
    $chat[ 'header' ] = t( 'Chat' )->__toString();

    $buddylist_online = chatNodeJSController::_chat_buddylist_online();
    $user_fields = User::load( $user->id() )->getFields( $include_computed = true );

    // Initial value when user first registered and not filled in his profile
    if ( isset( $user_fields['user_picture']->getValue()[ 0 ][ 'target_id' ] ) ) {

      $image_file_id = $user_fields['user_picture']->getValue()[ 0 ][ 'target_id' ];
      $image_file = File::load( $image_file_id );
      $image_url = file_create_url( $image_file->uri->value );

    } else {

      $image_url = $base_url . '/' . drupal_get_path( 'module', 'chat_nodejs' ) . '/css/themes/main/images/default_avatar.png';

    }

    $field_name = $user->getDisplayName();
    $chat[ 'contents' ] = '<div class="chat_options" data-toggle="collapse" data-target="#buddylist">
                                <img alt="" style="margin:0 8px 0 3px;padding:2px;float:left;height:24px;width:24px;" src="' . $image_url . '">
                                <a class="name ' . $user->id() . '">' . $field_name . '</a>
                                <a class="status chat_loading" style="right:0;"></a>
                           </div>';

    // Static active users list when page is reloaded
    $items = array();

    foreach ( $buddylist_online as $key => $value ) {

      if ( $key != 'total' ) {

        $items[] = array(
          '#markup' => '<img alt="" src="' . $value[ 'p' ] . '"><a class="' . $key . '">' . $value[ 'name' ] . '</a>',
          '#wrapper_attributes' => array( 'class' =>'status-0' )
        );

      }

    }

    if ( $items ) {

      $item_list = array(
        '#theme' => 'item_list',
        '#items' => $items,
        '#list_type' => 'ul',
        '#attributes' => array(
          'id' => 'buddylist',
          'class' => 'collapse in'
        )
      );

      $chat[ 'footer' ] = $item_list;

    } else {

      $no_user_item = array();
      $no_user_item[] = array(
        '#markup' => t('No contacts')->__toString(),
        '#wrapper_attributes' => [ 'class' => 'chatnousers' ],
      );
      $render_array = array(
        '#theme' => 'item_list',
        '#items' => $no_user_item,
        '#list_type' => 'ul',
        '#attributes' => array(
          'id' => 'buddylist',
          'class' => 'collapse in'
        )
      );

      $chat[ 'footer' ] = $render_array;

    }

    $chat['text'] = t('Online')->__toString() . ' - <span class="online-count">' . $buddylist_online['total'] . '</span>';

    $chat_subpanel = array(
      '#theme' => 'chat_nodejs_subpanel',
      '#subpanel' => $chat
    );

    return $chat_subpanel;

  }

  /**
	*  Implements alter user chat status
	*/
	public function chat_status() {

		$user = Drupal::currentUser();
		$connection = Drupal::database();

		// User current status (offline/online/busy)
		if (isset($_POST['status'])) {

		  $uid = isset($_POST['uid_offline']) ? Html::escape($_POST['uid_offline']) : $user->id();

			$connection->update('chat_nodejs_users')
			->fields( [
				'status' => Html::escape($_POST['status']),
			] )
			->condition('uid', $uid)
			->execute();

		}

		// User switched his status (online/busy)
		if ( isset( $_POST['user_online_busy'] ) ) {

		  $user_online_busy = json_decode($_POST['user_online_busy']);
		  $status=$user_online_busy->status;
		  $messageBody=$user_online_busy->messageBody;

		  // Validate received JSON data.
		  if (is_object($user_online_busy)
			  &&(intval($status) == 1 || intval($status) == 2)
			    &&is_array($messageBody)) {

			  // Sanitize validated messageBody value
			  // All values must be integer as they are id's of the users
			  $messageBody_sanitized = array_filter($messageBody, 'ctype_digit');

        $nodejs_message = ( object ) array(
          'channel' => 'chat_' . $user->id(),
          'broadcast' => FALSE,
          'type' => 'userSwitchedOnlineBusy',
          'callback' => 'chatNodejsMessageHandler',
          'data' =>
            json_encode(( object ) array(
              'messageBody' => $messageBody_sanitized,
            )),
          'status' => $status
        );

        nodejs_enqueue_message($nodejs_message);

		  }

		}

		// Notification sound controls
		if ( isset( $_POST['notification_sound'] ) ) {

			$connection->update( 'chat_nodejs_users' )
			->fields( [
				'sound' => Html::escape( $_POST['notification_sound'] ),
			] )
			->condition( 'uid', $user->id() )
			->execute();

			// User toggled his sound notifications
		  if ( isset( $_POST['switch_sound'] ) ) {

        $switch_sound = json_decode($_POST['switch_sound']);
        $messageBody=$switch_sound->messageBody;

        // Validate received JSON data.
        if (is_object($switch_sound)
          &&(intval($messageBody->command) == 1 || intval(($messageBody->command) == 2))
            &&($messageBody->action == 'Unmute sound' || $messageBody->action == 'Mute sound')
              &&($messageBody->sign == 'up' || $messageBody->sign == 'off')) {

          $nodejs_message = ( object ) array(
            'channel' => 'chat_' . $user->id(),
            'broadcast' => FALSE,
            'type' => 'switchSound',
            'callback' => 'chatNodejsMessageHandler',
            'data' =>
              json_encode(( object ) array(
                'messageBody' => $messageBody,
              ))
          );

          nodejs_enqueue_message($nodejs_message);

        }

		  }

		}

    // Chat state closed/opened
    if (isset($_POST['chat_closed'])) {

      $connection->update('chat_nodejs_users')
        ->fields([
            'chat_closed' => Html::escape($_POST['chat_closed']),
        ])
        ->condition('uid', $user->id())
        ->execute();

      $nodejs_message = (object) array(
        'channel' => 'chat_' . $user->id(),
        'broadcast' => FALSE,
        'type' => 'chatState',
        'callback' => 'chatNodejsMessageHandler',
        'data' =>
          json_encode(( object ) array(
            'messageBody' => Html::escape($_POST['chat_closed']),
          ))

      );

      nodejs_enqueue_message($nodejs_message);

    }

    return new JsonResponse("Done!");

	}

  /**
   *  Implements edit user own message operations
    *
    * @throws
   */
  public function chat_edit_message() {

    $user = Drupal::currentUser();
    $connection = Drupal::database();

    if ( isset( $_POST['delete_msg'] ) ) {

      $delete_msg = Html::escape( $_POST['delete_msg'] );
      $connection->update( 'chat_nodejs_msg' )
        ->fields( [
          'message' => t('Message deleted...'),
        ] )
        ->condition( 'message_id', $delete_msg )
        ->execute();

        if (isset($_POST['delete_msg_data'])) {

          $delete_msg_data = json_decode($_POST['delete_msg_data']);
          $receiver_id = $delete_msg_data->messageBody->receiver_id;
          $sender_id = $delete_msg_data->messageBody->sender_id;

          $nodejs_message = ( object ) array(
            'channel' => 'chat_' . $user->id(),
            'broadcast' => FALSE,
            'type' => 'userDeletedMessage',
            'callback' => 'chatNodejsMessageHandler',
            'data' =>
              json_encode(( object ) array(
                'messageBody' => ( object ) array(
                  'message_id' => $delete_msg,
                  'receiver_id' => $receiver_id,
                  'sender_id' => $sender_id
                )
              ))
          );

          nodejs_enqueue_message($nodejs_message);

        }

    }

    return new JsonResponse( 'Done!' );

  }

}
