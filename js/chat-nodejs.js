// ------- Chat actions and NodeJS response messages handling operations -------
(function($,Drupal,drupalSettings){

  var resetDelayOnPageReload,
    bodyTag=$("body");

  Drupal.chat_nodejs=Drupal.chat_nodejs||{};

  Drupal.behaviors.sendDrupalMessage={

    attach:function(context,settings){

      Drupal.Nodejs.callbacks.chatNodejsMessageHandler={

        callback:function(message){

          switch(message.type){

            case"newMessage":

              Drupal.chat_nodejs.processChatDataNodejs($.parseJSON(message.data));
              break;

            case"userOwnChatMessage":

              message.data.own=true;
              Drupal.chat_nodejs.processChatDataNodejs(message.data);
              break;

            case"userOnline":

              Drupal.chat_nodejs.processUserOnline(message.data);
              break;

            case"userOffline":

              Drupal.chat_nodejs.processUserOffline(message.data);
              break;

            case"statusChange":

              Drupal.chat_nodejs.processUserSwitchedOnlineBusy(message.data);
              break;

            case"switchSound":

              Drupal.chat_nodejs.processUserSwitchedSound(message.data);
              break;

            case"newUserOnline":

              Drupal.chat_nodejs.processNewUserOnline(message.data);
              break;

            case"userDelete":

              Drupal.chat_nodejs.processUserDelete(message.data);
              break;

            case"userMessageDelete":

              Drupal.chat_nodejs.processUserDeletedMessage(message.data);
              break;

            case"chatState":

              Drupal.chat_nodejs.processChatState(message.data);
              break

          }
        }
      };

      // User click status online/busy in chat settings
      bodyTag.on("click",".tooltipster-opts #tooltip_content li.chat_options",function(){

        var message,friend_uids=[];

        $("#buddylist li a").each(function(){
          friend_uids.push($(this).attr("class"))
        });
        message={
          messageBody:friend_uids,
          status:$(this).attr("class").split(" ")[1].split("-")[1]
        };
        $.post(drupalSettings.chat_nodejs["statusUrl"], {user_online_busy:JSON.stringify(message)});

        return false
      });

      // User click chat sound on/off
      bodyTag.on("click","#chat-chat-options-s",function(){

        if($(this).html()==='<a href="#">Mute sound</a><span class="glyphicon glyph-volume-off"></span>'){
          sound_switcher("Unmute sound",2,"up")
        }else{
          sound_switcher("Mute sound",1,"off")
        }

        return false
      });

    }
  };

  Drupal.chat_nodejs.processChatDataNodejs=function(data){

    var value=data,
      formattedCurrentTime=value.timestamp,
      chatboxtitle=value.own ? value["uid2"] : value["uid1"],
      output='<div class="chatboxusername">',
      class_deleted=value.message==="Message deleted..." ? "deleted " : "",
      jChatboxTitleContent=$("#chatbox_"+chatboxtitle+" .chatboxcontent");

    if(value.message.length>0&&!value.own){
      if(parseInt(drupalSettings.chat_nodejs["notificationSound"])===1&&parseInt(drupalSettings.chat_nodejs["chatClosed"])===0){
        var sound=Drupal.chat_nodejs.sound_loader();
        sound.playSound(drupalSettings.chat_nodejs["sound_new_message"])
      }
	    // Blinking browser window tab with "New message" notification
      if($("body").hasClass("hidden_")){
        flashTitle(drupalSettings.chat_nodejs["newMessage"])
      }

      // Open chat with the new message from another user
      if (!$("#mainpanel #chatbox_" + value["uid1"]).is(':visible')) {
        $("#chatpanel #buddylist li a." + value["uid1"]).click();
      }

    }

    value.message=Drupal.chat_nodejs.parseHTML(value.message);
    value.message=value.message.replace(/{line-break}/g,"<br />");
    output=output+'<div class="chatboxuserpicture"><img alt="" height="30" width="30" src="'+value.p+'" /></div>';
    output=output+'<span class="chatboxtime">'+formattedCurrentTime+'</span>' +
                  '<a href="'+drupalSettings.path["baseUrl"]+"user/"+value["uid1"]+'">'+(value.name.length>14?value.name.substring(0,13)+"...":value.name)+'</a></div><p class="'+class_deleted+value["message_id"]+'">'+value.message+"</p>";
    jChatboxTitleContent.append(output);

    if(jChatboxTitleContent[0]){
      jChatboxTitleContent.scrollTop(jChatboxTitleContent[0].scrollHeight)
    }

  };

  // Process user status changed to offline
  Drupal.chat_nodejs.processUserOffline=function(uid){

    if(uid&&$("#buddylist li a").hasClass(uid)){
      if(!resetDelayOnPageReload){
        resetDelayOnPageReload=setTimeout(function(){
          var jStatus=$("#buddylist li a."+uid).closest("li");

          switch(jStatus.attr("class")){

            case"status-1":

              jStatus.removeClass("status-1").addClass("status-0");
              $("#chatbox_"+uid+" .subpanel_title div.status-1").removeClass("status-1").addClass("status-0");
              break;

            case"status-2":

              jStatus.removeClass("status-2").addClass("status-0");
              $("#chatbox_"+uid+" .subpanel_title div.status-2").removeClass("status-2").addClass("status-0");
              break

          }

          // Update user status in DB
          $.post(drupalSettings.chat_nodejs["statusUrl"],{uid_offline:uid,status:0});

          reset_user_status_in_text_box();
          $("#chatpanel .online-count").html($("#buddylist li:not(.status-0)").length);
          resetDelayOnPageReload=undefined;

        },1e4)
      }
    }
  };

  // Process user status changed online/busy
  Drupal.chat_nodejs.processUserOnline=function(data){

	  // Status change indication in own chat
    if(!("status"in data)){
      var jChatLoading=$(".chat_options .chat_loading");
      if(!$.isEmptyObject(data)){
        var current_uid=$("#chatpanel .chat_options a.name").attr("class").split(" ")[1];
        for(var id in data){
          if(current_uid!==id){
            var _jStatus=$("#buddylist li a."+id).closest("li");
				    _jStatus.removeClass("status-0").addClass("status-"+data[id]);
            $("#chatbox_"+id+" .subpanel_title div.status-0").removeClass("status-0").addClass("status-"+data[id])
          }
        }
        if(!$("#chatpanel .subpanel #buddylist li.chatnousers").length){
          $("#chatpanel .online-count").html($("#buddylist li:not(.status-0)").length);
        }
      }

      switch(drupalSettings.chat_nodejs.status){

        case"1":

          jChatLoading.removeClass("chat_loading").addClass("status-1");
          break;

        case"2":

          jChatLoading.removeClass("chat_loading").addClass("status-2");
          break

      }

	  // Status change indication in chat of the friend who is currently online
    }else{
      var jStatus=$("#buddylist li a."+data.uid).closest("li");
      if(resetDelayOnPageReload){
        clearTimeout(resetDelayOnPageReload);
        resetDelayOnPageReload=undefined;
      }
      if(jStatus.attr("class")==="status-0"){
        jStatus.removeClass("status-0").addClass("status-"+data.status);
        $("#chatbox_"+data.uid+" .subpanel_title div.status-0").removeClass("status-0").addClass("status-"+data.status)
      }
      if(!$("#chatpanel .subpanel #buddylist li.chatnousers").length){
        $("#chatpanel .online-count").html($("#buddylist li:not(.status-0)").length);
      }
    }

    reset_user_status_in_text_box();

  };

  Drupal.chat_nodejs.processUserSwitchedOnlineBusy=function(data){

    if($("#chatpanel .subpanel .chat_options a.name").hasClass(data.id)){
      $("#chatpanel .subpanel .chat_options a.status").removeClass().addClass("status status-"+data.status)
    }else{
      var _jStatus=$("#buddylist li a."+data.id).closest("li"),
        _jStatusSubpanelTitle=$("#chatbox_"+data.id+" .subpanel_title div");
      _jStatus.removeClass(_jStatus.attr("class")).addClass("status-"+data.status);
      _jStatusSubpanelTitle.removeClass(_jStatusSubpanelTitle.attr("class")).addClass("status-"+data.status)
    }

  };

  Drupal.chat_nodejs.processNewUserOnline=function(data){

    var has_no_users=$("#chatpanel .subpanel #buddylist li.chatnousers"),
      existing_users = data.users;

    // Delete li.chatnousers before add the first item to buddylist
    if(has_no_users.length){
      has_no_users.remove()
    }

    if (!existing_users) {
      add_user(data.id,data.status,data.name,data.src);
    } else {
      $.each(data.users, function (id, status) {
          add_user(id,status,data.user_data[id]['name'],data.user_data[id]['src']);
      });
    }

    $("#chatpanel .online-count").html($("#buddylist li:not(.status-0)").length)

  };

  Drupal.chat_nodejs.processUserDelete=function(data){

    if (data.action === 'delete') {
      // Delete Chat of the deleted user
      $('#chat-wrapper').remove();
      // Clean up Cookies of the Chat NodeJS module
      Drupal.chat_nodejs.Cookie.delete("chat_open_chat_uids");
      Drupal.chat_nodejs.Cookie.delete("chat_current_open_chat_window_ids");
      Drupal.chat_nodejs.Cookie.delete("chat_nodejs_login")
    } else {
      $("#chatpanel .subpanel #"+data.list+" li a."+data.uid).closest("li").hide("slow",function(){

        var buddylist=$("#chatpanel .subpanel #buddylist li:not(.status-0)"),
          buddylist_all=$("#chatpanel .subpanel #buddylist li"),
          has_no_users=$("#chatpanel .subpanel #buddylist li.chatnousers");

        $(this).remove();

        // Set amount of online users in friend list
        if(!buddylist_all.length&&!has_no_users.length){
          setTimeout(function(){
            $("#chatpanel .subpanel #buddylist").append(drupalSettings.chat_nodejs["noUsers"]).hide().fadeIn(1e3);
            $("#chatpanel .online-count").html("0")
          },1e3)
        } else {
          $("#chatpanel .online-count").html(buddylist.length-has_no_users.length);
        }

      });

      $("#chat #chatbox_"+data.uid).remove();
    }

  };

  Drupal.chat_nodejs.processUserDeletedMessage=function(message_data){

    $("#chatbox_"+message_data.chatbox_id+" .chatboxcontent")
      .find("p."+message_data.message_id)
        .attr("class","deleted")
          .text("Message deleted...")
            .hide()
              .fadeIn("slow")

  };

  Drupal.chat_nodejs.processChatState=function(state) {

    if (state['state']==='1') {
      $("#chat-wrapper").fadeOut(2e3,"swing",function(){
        $("#call-messenger").fadeIn(2e3,"swing");
      });
    }

    if (state['state']==='0') {
      $("#call-messenger").fadeOut(2e3,"swing",function(){
        $("#chat-wrapper").fadeIn(2e3,"swing");
      });
    }

  };

	// Manipulate with sound switcher in the currently openned browser window
	// If more than one window has been openned all windows (except of current) manipulated from the "Main chat window options" tooltip functionInit
  Drupal.chat_nodejs.processUserSwitchedSound=function(sound_state){

    $("#chat-chat-options-s").html('<a href="#">'+sound_state.action+'</a><span class="glyphicon glyph-volume-'+sound_state.sign+'"></span>');
    // Save current state of the sound notifications to reuse without the page reload
    // Do not delete!!!
    // This raw is required when more then 1 window tab is openned
    drupalSettings.chat_nodejs["notificationSound"]=sound_state.command

  };

  function reset_user_status_in_text_box(){

    $('li[id^="chatbox_"]').each(function(){

      var user_online=$("#chatpanel .subpanel .item-list li a."+this.id.split("_")[1]).closest("li");
      if(user_online.hasClass("status-0")){
        Drupal.chat_nodejs.changeStatus(this.id,0)
      }else if(user_online.hasClass("status-1")){
        Drupal.chat_nodejs.changeStatus(this.id,1)
      }else if(user_online.hasClass("status-2")){
        Drupal.chat_nodejs.changeStatus(this.id,2)
      }

    })

  }

  function sound_switcher(action,_command,sign){

    var message={
			messageBody:{action:action,
                         sign:sign,
                         command:_command
                        }
		};

    $.post(drupalSettings.chat_nodejs["statusUrl"],{notification_sound:_command, switch_sound:JSON.stringify(message)});
	  // Save current state of the sound notifications to reuse without the page reload
	  // Do not delete!!!
	  // This raw is required
	  drupalSettings.chat_nodejs["notificationSound"]=_command

  }

  function add_user(id,status,name,src) {

    $("#chatpanel .subpanel #buddylist").append(
      '<li class="status-'+status+'">' +
      '<img alt="" width="24" height="24" src="'+src+'">' +
      '<a class="'+id+'">'+name+'</a>' +
      '</li>');

  }

})(jQuery,Drupal,drupalSettings);
