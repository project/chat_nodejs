(function($,Drupal,drupalSettings){

  var chat_path_visibility = $("input[name=chat_path_visibility]");

  chat_path_visibility.change(function() {

    var chat_path_visibility_checked_val = $("input[name=chat_path_visibility]:checked").val();

    if ((parseInt(chat_path_visibility_checked_val) === 1) || (parseInt(chat_path_visibility_checked_val) === 2)) {
      $(".form-item-chat-path-pages").show();
    }
    else {
      $(".form-item-chat-path-pages").hide();
    }

  });

  chat_path_visibility.change();

})(jQuery,Drupal,drupalSettings);
