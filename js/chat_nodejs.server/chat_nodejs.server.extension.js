let chat_users={},
  user_out=0;

exports.setup=function(clientManager){

  process.on("client-authenticated",function(sessionId,authData){

    let session_uids=clientManager.getNodejsSessionIdsFromUid(clientManager.sockets[sessionId].uid);

    console.log("Auth - "+authData.uid);

    if(parseInt(authData.uid)!==0){

      let message={messageType:"createChannel",uid:authData.uid};

      clientManager.backend.sendMessageToBackend(message,function(error,response,friend_list){

        let uids=JSON.parse(JSON.parse(friend_list)),
          friend_list_online={};

        if(error){
          console.log("Error sending message to backend.",error);
          return}

        chat_users[authData.uid]=sessionId;
        console.log("Added - "+authData.uid);

        // Update own chat status (even if you have an empty friend list)
        publish_message_to_client(session_uids,{type:"userOnline",data:{id:authData.uid},callback:"chatNodejsMessageHandler"},clientManager);

        if(Object.keys(uids).length!==0&&uids.constructor===Object){

          for(let id in uids){
            for(let uid in chat_users){
              if(id===uid){
                friend_list_online[id]=uids[id];
                break
              }
            }
          }

          // Update own friend list and show who is currently online
          publish_message_to_client(session_uids,{type:"userOnline",data:friend_list_online,callback:"chatNodejsMessageHandler"},clientManager);
          // ids of the own friends who are currently online
          let user_ids=Object.keys(friend_list_online);
          user_ids.forEach(function(id){
            if(id!==authData.uid){
              // Update own presence in friend list of the currently online friend
              publish_message_to_client(clientManager.getNodejsSessionIdsFromUid(id),{type:"userOnline",data:{uid:authData.uid,status:friend_list_online[authData.uid]},callback:"chatNodejsMessageHandler"},clientManager)
            }
          })
        }
      })
    }
  })
    .on("message-published",function(message){

      let session_uids,
        message_data=JSON.parse(message.data);

      switch(message.type){

        case"newMessage":

          session_uids=clientManager.getNodejsSessionIdsFromUid(message_data.uid1);

          publish_message_to_client(session_uids,{type:"userOwnChatMessage",data:message_data,callback:"chatNodejsMessageHandler"},clientManager);
          break;

        case"userSwitchedOnlineBusy":

          let buddy=message.channel.replace( /^\D+/g, '');

          session_uids=clientManager.getNodejsSessionIdsFromUid(message.channel.replace( /^\D+/g, ''));

          publish_message_to_client(session_uids,{type:"statusChange",data:{status:message.status,id:buddy},callback:"chatNodejsMessageHandler"},clientManager);
          message_data.messageBody.forEach(function(uid){
            publish_message_to_client(clientManager.getNodejsSessionIdsFromUid(uid),{type:"statusChange",data:{status:message.status,id:buddy},callback:"chatNodejsMessageHandler"},clientManager)
          });
          break;

        case "switchSound":

          session_uids=clientManager.getNodejsSessionIdsFromUid(message.channel.replace( /^\D+/g, ''));

          publish_message_to_client(session_uids,{type:"switchSound",data:{action:message_data.messageBody.action,sign:message_data.messageBody.sign,command:message_data.messageBody.command},callback:"chatNodejsMessageHandler"},clientManager);
          break;

        case"newUserUpdates":

          let users=message_data.messageBody.users,
            newUID=message.channel.replace( /^\D+/g, ''),
            send_request={messageType:"getUserData",uids:Object.keys(users)};

          clientManager.backend.sendMessageToBackend(send_request,function(error,response,userData){

            let user_data=JSON.parse(JSON.parse(userData));

            if(error){
              console.log("Error sending message to backend.",error);
              return;}

            for(let id in users){

              // Update Chats of the existing users with data of the new one
              if(id!==newUID){
                publish_message_to_client(clientManager.getNodejsSessionIdsFromUid(id),{
                  type:"newUserOnline",
                  data:{id:newUID,status:users[newUID],name:user_data[newUID]['name'],src:user_data[newUID]['src']},
                  callback:"chatNodejsMessageHandler"},clientManager)
                // Update Chat of the new user
              } else {
                session_uids=clientManager.getNodejsSessionIdsFromUid(newUID);
                delete users[newUID];
                publish_message_to_client(session_uids,{
                  type:"newUserOnline",
                  data:{users:users,user_data:user_data},
                  callback:"chatNodejsMessageHandler"},clientManager);
              }

            }

          });
          break;

        case"userDeleted":

          // Update Chats of the existing users
          for(let id in chat_users){
            if (id!==message_data.messageBody.id1) {
              publish_message_to_client(clientManager.getNodejsSessionIdsFromUid(id),{
                type:"userDelete",
                data:{uid:message_data.messageBody.id1,list:message.list},
                callback:"chatNodejsMessageHandler"},clientManager);
            }
          }

          // Delete Chat of the deleted user
          publish_message_to_client(clientManager.getNodejsSessionIdsFromUid(message_data.messageBody.id1),{
            type:"userDelete",
            data:{action:'delete'},
            callback:"chatNodejsMessageHandler"},clientManager);

          clientManager.kickUser(message_data.messageBody.id1);
          break;

        case"userDeletedMessage":

          session_uids=clientManager.getNodejsSessionIdsFromUid(message.channel.replace( /^\D+/g, ''));

          // Own Chat update
          publish_message_to_client(session_uids,{type:"userMessageDelete",data:{message_id:message_data.messageBody.message_id,chatbox_id:message_data.messageBody.receiver_id},callback:"chatNodejsMessageHandler"},clientManager);
          // Friend Chat update
          publish_message_to_client(clientManager.getNodejsSessionIdsFromUid(message_data.messageBody.receiver_id),{type:"userMessageDelete",data:{message_id:message_data.messageBody.message_id,chatbox_id:message_data.messageBody.sender_id},callback:"chatNodejsMessageHandler"},clientManager);
          break;

        case"chatState":

          session_uids=clientManager.getNodejsSessionIdsFromUid(message.channel.replace( /^\D+/g, ''));

          // Own Chat update
          publish_message_to_client(session_uids,{type:"chatState",data:{state:message_data.messageBody},callback:"chatNodejsMessageHandler"},clientManager);
          break;

      }

    })
    .on("client-disconnect",function(sessionId){

      let session_uids,
        client = clientManager.sockets[sessionId];

      if(client){

        user_out=client.uid;
        session_uids=clientManager.getNodejsSessionIdsFromUid(user_out);

        console.log("Out - "+user_out);

        if(session_uids.length-1===0){
          delete chat_users[user_out];
          for(let id in chat_users){
            publish_message_to_client(clientManager.getNodejsSessionIdsFromUid(id),{type:"userOffline",data:user_out,callback:"chatNodejsMessageHandler"},clientManager)
          }
        }

      }

    })

};

function publish_message_to_client(session_arr,prop_object,clientManager){

  session_arr.forEach(function(session_id){
    clientManager.publishMessageToClient(session_id,prop_object)
  })

}
