(function($,Drupal,drupalSettings){

  var jChat=$("#chat"),
    bodyTag=$("body"),
    hide_delay,
    original_title=document.title,
    timeout_flash_title,
    hidden="hidden",

    // To prevent multiple calls of the function getThreadHistory for old messages
    // Allow to call it next time only after AJAX post request is finished
    old_history=false,

    // Delay before buddylist update
    buddylist_update_timeout=0,

    // Main Chat object
    chat={
      username:null,
      uid:null,
      send_current_message:null,
      send_current_message_id:null,
      last_timestamp:0,
      send_current_uid2:0,
      attach_messages_in_queue:0,
      running:0,
      online_users:0,
      open_chat_uids:{},
      current_open_chat_window_ids:{}
    },

    // Emoji vars
    activeEl,
    emojiContainer="emojionepicker",
    emojiList={
      people:{
        header:"1f603",
        content:"1f302 1f383 1f385 1f392 1f393 1f3a9 1f3c3-2640 1f3c3-2642 1f3c3 1f440 1f441 1f442 1f443 1f444 1f445 1f446 1f447 1f448 1f449 1f44a 1f44b 1f44c 1f44d 1f44e 1f44f 1f450 1f451 1f452 1f453 1f454 1f455 1f456 1f457 1f458 1f459 1f45a 1f45b 1f45c 1f45d 1f45e 1f45f 1f460 1f461 1f462 1f463 1f464 1f465 1f466 1f467 1f468-1f33e 1f468-1f373 1f468-1f393 1f468-1f3a4 1f468-1f3a8 1f468-1f3eb 1f468-1f3ed 1f468-1f466-1f466 1f468-1f466 1f468-1f467-1f466 1f468-1f467-1f467 1f468-1f467 1f468-1f468-1f466-1f466 1f468-1f468-1f466 1f468-1f468-1f467-1f466 1f468-1f468-1f467-1f467 1f468-1f468-1f467 1f468-1f469-1f466-1f466 1f468-1f469-1f466 1f468-1f469-1f467-1f466 1f468-1f469-1f467-1f467 1f468-1f469-1f467 1f468-1f4bb 1f468-1f4bc 1f468-1f527 1f468-1f52c 1f468-1f680 1f468-1f692 1f468-1f9b0 1f468-1f9b1 1f468-1f9b2 1f468-1f9b3 1f468-2695 1f468-2696 1f468-2708 1f468-2764-1f468 1f468-2764-1f48b-1f468 1f468 1f469-1f33e 1f469-1f373 1f469-1f393 1f469-1f3a4 1f469-1f3a8 1f469-1f3eb 1f469-1f3ed 1f469-1f466-1f466 1f469-1f466 1f469-1f467-1f466 1f469-1f467-1f467 1f469-1f467 1f469-1f469-1f466-1f466 1f469-1f469-1f466 1f469-1f469-1f467-1f466 1f469-1f469-1f467-1f467 1f469-1f469-1f467 1f469-1f4bb 1f469-1f4bc 1f469-1f527 1f469-1f52c 1f469-1f680 1f469-1f692 1f469-1f9b0 1f469-1f9b1 1f469-1f9b2 1f469-1f9b3 1f469-2695 1f469-2696 1f469-2708 1f469-2764-1f468 1f469-2764-1f469 1f469-2764-1f48b-1f468 1f469-2764-1f48b-1f469 1f469 1f46a 1f46b 1f46c 1f46d 1f46e-2640 1f46e-2642 1f46e 1f46f-2640 1f46f-2642 1f46f 1f470 1f471-2640 1f471-2642 1f471 1f472 1f473-2640 1f473-2642 1f473 1f474 1f475 1f476 1f477-2640 1f477-2642 1f477 1f478 1f479 1f47a 1f47b 1f47c 1f47d 1f47e 1f47f 1f480 1f481-2640 1f481-2642 1f481 1f482-2640 1f482-2642 1f482 1f483 1f484 1f485 1f486-2640 1f486-2642 1f486 1f487-2640 1f487-2642 1f487 1f48b 1f48d 1f48f 1f491 1f4a9 1f4aa 1f4bc 1f574 1f575-2640 1f575-2642 1f575 1f576 1f57a 1f590 1f595 1f596 1f5e3 1f600 1f601 1f602 1f603 1f604 1f605 1f606 1f607 1f608 1f609 1f60a 1f60b 1f60c 1f60d 1f60e 1f60f 1f610 1f611 1f612 1f613 1f614 1f615 1f616 1f617 1f618 1f619 1f61a 1f61b 1f61c 1f61d 1f61e 1f61f 1f620 1f621 1f622 1f623 1f624 1f625 1f626 1f627 1f628 1f629 1f62a 1f62b 1f62c 1f62d 1f62e 1f62f 1f630 1f631 1f632 1f633 1f634 1f635 1f636 1f637 1f638 1f639 1f63a 1f63b 1f63c 1f63d 1f63e 1f63f 1f640 1f641 1f642 1f643 1f644 1f645-2640 1f645-2642 1f645 1f646-2640 1f646-2642 1f646 1f647-2640 1f647-2642 1f647 1f64b-2640 1f64b-2642 1f64b 1f64c 1f64d-2640 1f64d-2642 1f64d 1f64e-2640 1f64e-2642 1f64e 1f64f 1f6b6-2640 1f6b6-2642 1f6b6 1f910 1f911 1f912 1f913 1f914 1f915 1f916 1f917 1f918 1f919 1f91a 1f91b 1f91c 1f91d 1f91e 1f91f 1f920 1f921 1f922 1f923 1f924 1f925 1f926-2640 1f926-2642 1f926 1f927 1f928 1f929 1f92a 1f92b 1f92c 1f92d 1f92e 1f92f 1f930 1f931 1f932 1f933 1f934 1f935 1f936 1f937-2640 1f937-2642 1f937 1f970 1f973 1f974 1f975 1f976 1f97a 1f97c 1f97d 1f97e 1f97f 1f9b0 1f9b1 1f9b2 1f9b3 1f9b4 1f9b5 1f9b6 1f9b7 1f9b8-2640 1f9b8-2642 1f9b8 1f9b9-2640 1f9b9-2642 1f9b9 1f9d0 1f9d1 1f9d2 1f9d3 1f9d4 1f9d5 1f9d6-2640 1f9d6-2642 1f9d6 1f9d9-2640 1f9d9-2642 1f9d9 1f9da-2640 1f9da-2642 1f9da 1f9db-2640 1f9db-2642 1f9db 1f9dc-2640 1f9dc-2642 1f9dc 1f9dd-2640 1f9dd-2642 1f9dd 1f9de-2640 1f9de-2642 1f9de 1f9df-2640 1f9df-2642 1f9df 1f9e0 1f9e2 1f9e3 1f9e4 1f9e5 1f9e6 1f9f3 1f9f5 1f9f6 261d 2620 2639 263a 26d1 270a 270b 270c 270d".split(" ")
      },
      nature:{
        header:"1f340",
        content:"1f308 1f30a 1f30d 1f30e 1f30f 1f311 1f312 1f313 1f314 1f315 1f316 1f317 1f318 1f319 1f31a 1f31b 1f31c 1f31d 1f31e 1f31f 1f324 1f325 1f326 1f327 1f328 1f329 1f32a 1f32b 1f32c 1f331 1f332 1f333 1f334 1f335 1f337 1f338 1f339 1f33a 1f33b 1f33c 1f33e 1f33f 1f340 1f341 1f342 1f343 1f344 1f384 1f38b 1f38d 1f400 1f401 1f402 1f403 1f404 1f405 1f406 1f407 1f408 1f409 1f40a 1f40b 1f40c 1f40d 1f40e 1f40f 1f410 1f411 1f412 1f413 1f414 1f415 1f416 1f417 1f418 1f419 1f41a 1f41b 1f41c 1f41d 1f41e 1f41f 1f420 1f421 1f422 1f423 1f424 1f425 1f426 1f427 1f428 1f429 1f42a 1f42b 1f42c 1f42d 1f42e 1f42f 1f430 1f431 1f432 1f433 1f434 1f435 1f436 1f437 1f438 1f439 1f43a 1f43b 1f43c 1f43d 1f43e 1f43f 1f490 1f4a5 1f4a6 1f4a7 1f4a8 1f4ab 1f525 1f54a 1f577 1f578 1f648 1f649 1f64a 1f940 1f980 1f981 1f982 1f983 1f984 1f985 1f986 1f987 1f988 1f989 1f98a 1f98b 1f98c 1f98d 1f98e 1f98f 1f990 1f991 1f992 1f993 1f994 1f995 1f996 1f997 1f998 1f999 1f99a 1f99b 1f99c 1f99d 1f99e 1f99f 1f9a0 1f9a1 1f9a2 2600 2601 2602 2603 2604 2614 2618 26a1 26c4 26c5 26c8 2728 2744 2b50".split(" ")
      },
      food:{
        header:"1f354",
        content:"1f32d 1f32e 1f32f 1f330 1f336 1f33d 1f345 1f346 1f347 1f348 1f349 1f34a 1f34b 1f34c 1f34d 1f34e 1f34f 1f350 1f351 1f352 1f353 1f354 1f355 1f356 1f357 1f358 1f359 1f35a 1f35b 1f35c 1f35d 1f35e 1f35f 1f360 1f361 1f362 1f363 1f364 1f365 1f366 1f367 1f368 1f369 1f36a 1f36b 1f36c 1f36d 1f36e 1f36f 1f370 1f371 1f372 1f373 1f374 1f375 1f376 1f377 1f378 1f379 1f37a 1f37b 1f37c 1f37d 1f37e 1f37f 1f382 1f942 1f943 1f944 1f950 1f951 1f952 1f953 1f954 1f955 1f956 1f957 1f958 1f959 1f95a 1f95b 1f95c 1f95d 1f95e 1f95f 1f960 1f961 1f962 1f963 1f964 1f965 1f966 1f967 1f968 1f969 1f96a 1f96b 1f96c 1f96d 1f96e 1f96f 1f9b4 1f9c0 1f9c1 1f9c2 2615".split(" ")
      },
      objects:{
        header:"2696",
        content:"1f321 1f380 1f381 1f388 1f389 1f38a 1f38e 1f38f 1f390 1f399 1f39a 1f39b 1f39e 1f3a5 1f3ee 1f3f7 1f3fa 1f488 1f489 1f48a 1f48c 1f48e 1f4a1 1f4a3 1f4b0 1f4b3 1f4b4 1f4b5 1f4b6 1f4b7 1f4b8 1f4bb 1f4bd 1f4be 1f4bf 1f4c0 1f4c1 1f4c2 1f4c3 1f4c4 1f4c5 1f4c6 1f4c7 1f4c8 1f4c9 1f4ca 1f4cb 1f4cc 1f4cd 1f4ce 1f4cf 1f4d0 1f4d1 1f4d2 1f4d3 1f4d4 1f4d5 1f4d6 1f4d7 1f4d8 1f4d9 1f4da 1f4dc 1f4dd 1f4de 1f4df 1f4e0 1f4e1 1f4e4 1f4e5 1f4e6 1f4e7 1f4e8 1f4e9 1f4ea 1f4eb 1f4ec 1f4ed 1f4ee 1f4ef 1f4f0 1f4f1 1f4f2 1f4f7 1f4f8 1f4f9 1f4fa 1f4fb 1f4fc 1f4fd 1f4ff 1f50b 1f50c 1f50d 1f50e 1f50f 1f510 1f511 1f512 1f513 1f516 1f517 1f526 1f527 1f528 1f529 1f52a 1f52b 1f52c 1f52d 1f52e 1f56f 1f570 1f573 1f579 1f587 1f58a 1f58b 1f58c 1f58d 1f5a5 1f5a8 1f5b1 1f5b2 1f5bc 1f5c2 1f5c3 1f5c4 1f5d1 1f5d2 1f5d3 1f5dc 1f5dd 1f5de 1f5e1 1f5f3 1f6aa 1f6ac 1f6b0 1f6bd 1f6bf 1f6c0 1f6c1 1f6cb 1f6cc 1f6cd 1f6ce 1f6cf 1f6d2 1f6e0 1f6e1 1f6e2 1f9a0 1f9e7 1f9e8 1f9e9 1f9ea 1f9eb 1f9ec 1f9ed 1f9ee 1f9ef 1f9f0 1f9f1 1f9f2 1f9f4 1f9f5 1f9f6 1f9f7 1f9f8 1f9f9 1f9fa 1f9fb 1f9fc 1f9fd 1f9fe 1f9ff 231a 231b 2328 23f0 23f1 23f2 23f3 260e 265f 2692 2694 2696 2697 2699 26b0 26b1 26cf 26d3 2702 2709 270f 2712".split(" ")
      },
      activity:{
        header:"1f3c8",
        content:"1f396 1f397 1f39f 1f3a3 1f3a4 1f3a7 1f3a8 1f3aa 1f3ab 1f3ac 1f3ad 1f3ae 1f3af 1f3b0 1f3b1 1f3b2 1f3b3 1f3b7 1f3b8 1f3b9 1f3ba 1f3bb 1f3bc 1f3bd 1f3be 1f3bf 1f3c0 1f3c2 1f3c4-2640 1f3c4-2642 1f3c4 1f3c5 1f3c6 1f3c7 1f3c8 1f3c9 1f3ca-2640 1f3ca-2642 1f3ca 1f3cb-2640 1f3cb-2642 1f3cb 1f3cc-2640 1f3cc-2642 1f3cc 1f3cf 1f3d0 1f3d1 1f3d2 1f3d3 1f3f5 1f3f8 1f3f9 1f6a3-2640 1f6a3-2642 1f6a3 1f6b4-2640 1f6b4-2642 1f6b4 1f6b5-2640 1f6b5-2642 1f6b5 1f6f7 1f6f9 1f938-2640 1f938-2642 1f938 1f939-2640 1f939-2642 1f939 1f93a 1f93c-2640 1f93c-2642 1f93c 1f93d-2640 1f93d-2642 1f93d 1f93e-2640 1f93e-2642 1f93e 1f941 1f945 1f947 1f948 1f949 1f94a 1f94b 1f94c 1f94d 1f94e 1f94f 1f9d7-2640 1f9d7-2642 1f9d7 1f9d8-2640 1f9d8-2642 1f9d8 1f9e9 265f 26bd 26be 26f3 26f7 26f8 26f9-2640 26f9-2642 26f9".split(" ")
      },
      travel:{
        header:"1f697",
        content:"1f301 1f303 1f304 1f305 1f306 1f307 1f309 1f30b 1f30c 1f320 1f386 1f387 1f391 1f3a0 1f3a1 1f3a2 1f3cd 1f3ce 1f3d4 1f3d5 1f3d6 1f3d7 1f3d8 1f3d9 1f3da 1f3db 1f3dc 1f3dd 1f3de 1f3df 1f3e0 1f3e1 1f3e2 1f3e3 1f3e4 1f3e5 1f3e6 1f3e8 1f3e9 1f3ea 1f3eb 1f3ec 1f3ed 1f3ef 1f3f0 1f492 1f4ba 1f54b 1f54c 1f54d 1f5fa 1f5fb 1f5fc 1f5fd 1f5fe 1f5ff 1f680 1f681 1f682 1f683 1f684 1f685 1f686 1f687 1f688 1f689 1f68a 1f68b 1f68c 1f68d 1f68e 1f68f 1f690 1f691 1f692 1f693 1f694 1f695 1f696 1f697 1f698 1f699 1f69a 1f69b 1f69c 1f69d 1f69e 1f69f 1f6a0 1f6a1 1f6a2 1f6a4 1f6a5 1f6a6 1f6a7 1f6a8 1f6b2 1f6e3 1f6e4 1f6e5 1f6e9 1f6eb 1f6ec 1f6f0 1f6f3 1f6f4 1f6f5 1f6f6 1f6f8 1f9e8 1f9f3 2693 26e9 26ea 26f0 26f1 26f2 26f4 26f5 26fa 26fd 2708".split(" ")
      },
      symbols:{
        header:"2622",
        content:"0023-20e3 0023 002a-20e3 002a 0030-20e3 0030 0031-20e3 0031 0032-20e3 0032 0033-20e3 0033 0034-20e3 0034 0035-20e3 0035 0036-20e3 0036 0037-20e3 0037 0038-20e3 0038 0039-20e3 0039 00a9 00ae 1f004 1f0cf 1f170 1f171 1f17e 1f17f 1f18e 1f191 1f192 1f193 1f194 1f195 1f196 1f197 1f198 1f199 1f19a 1f201 1f202 1f21a 1f22f 1f232 1f233 1f234 1f235 1f236 1f237 1f238 1f239 1f23a 1f250 1f251 1f300 1f310 1f3a6 1f3b4 1f3b5 1f3b6 1f3e7 1f441-1f5e8 1f493 1f494 1f495 1f496 1f497 1f498 1f499 1f49a 1f49b 1f49c 1f49d 1f49e 1f49f 1f4a0 1f4a2 1f4a4 1f4ac 1f4ad 1f4ae 1f4af 1f4b1 1f4b2 1f4b9 1f4db 1f4e2 1f4e3 1f4f3 1f4f4 1f4f5 1f4f6 1f500 1f501 1f502 1f503 1f504 1f505 1f506 1f507 1f508 1f509 1f50a 1f514 1f515 1f518 1f519 1f51a 1f51b 1f51c 1f51d 1f51e 1f51f 1f520 1f521 1f522 1f523 1f524 1f52f 1f530 1f531 1f532 1f533 1f534 1f535 1f536 1f537 1f538 1f539 1f53a 1f53b 1f53c 1f53d 1f549 1f54e 1f550 1f551 1f552 1f553 1f554 1f555 1f556 1f557 1f558 1f559 1f55a 1f55b 1f55c 1f55d 1f55e 1f55f 1f560 1f561 1f562 1f563 1f564 1f565 1f566 1f567 1f5a4 1f5e8 1f5ef 1f6ab 1f6ad 1f6ae 1f6af 1f6b1 1f6b3 1f6b7 1f6b8 1f6b9 1f6ba 1f6bb 1f6bc 1f6be 1f6c2 1f6c3 1f6c4 1f6c5 1f6d0 1f6d1 1f9e1 203c 2049 2122 2139 2194 2195 2196 2197 2198 2199 21a9 21aa 23cf 23e9 23ea 23eb 23ec 23ed 23ee 23ef 23f8 23f9 23fa 24c2 25aa 25ab 25b6 25c0 25fb 25fc 25fd 25fe 2611 2622 2623 2626 262a 262e 262f 2638 2640 2642 2648 2649 264a 264b 264c 264d 264e 264f 2650 2651 2652 2653 2660 2663 2665 2666 2668 267b 267e 267f 2695 269b 269c 26a0 26aa 26ab 26ce 26d4 2705 2714 2716 271d 2721 2733 2734 2747 274c 274e 2753 2754 2755 2757 2763 2764 2795 2796 2797 27a1 27b0 27bf 2934 2935 2b05 2b06 2b07 2b1b 2b1c 2b55 3030 303d 3297 3299".split(" ")
      },
      flags:{
        header:"1f3f4-2620",
        content:"1f1e6-1f1e8 1f1e6-1f1e9 1f1e6-1f1ea 1f1e6-1f1eb 1f1e6-1f1ec 1f1e6-1f1ee 1f1e6-1f1f1 1f1e6-1f1f2 1f1e6-1f1f4 1f1e6-1f1f6 1f1e6-1f1f7 1f1e6-1f1f8 1f1e6-1f1f9 1f1e6-1f1fa 1f1e6-1f1fc 1f1e6-1f1fd 1f1e6-1f1ff 1f1e7-1f1e6 1f1e7-1f1e7 1f1e7-1f1e9 1f1e7-1f1ea 1f1e7-1f1eb 1f1e7-1f1ec 1f1e7-1f1ed 1f1e7-1f1ee 1f1e7-1f1ef 1f1e7-1f1f1 1f1e7-1f1f2 1f1e7-1f1f3 1f1e7-1f1f4 1f1e7-1f1f6 1f1e7-1f1f7 1f1e7-1f1f8 1f1e7-1f1f9 1f1e7-1f1fb 1f1e7-1f1fc 1f1e7-1f1fe 1f1e7-1f1ff 1f1e8-1f1e6 1f1e8-1f1e8 1f1e8-1f1e9 1f1e8-1f1eb 1f1e8-1f1ec 1f1e8-1f1ed 1f1e8-1f1ee 1f1e8-1f1f0 1f1e8-1f1f1 1f1e8-1f1f2 1f1e8-1f1f3 1f1e8-1f1f4 1f1e8-1f1f5 1f1e8-1f1f7 1f1e8-1f1fa 1f1e8-1f1fb 1f1e8-1f1fc 1f1e8-1f1fd 1f1e8-1f1fe 1f1e8-1f1ff 1f1e9-1f1ea 1f1e9-1f1ec 1f1e9-1f1ef 1f1e9-1f1f0 1f1e9-1f1f2 1f1e9-1f1f4 1f1e9-1f1ff 1f1ea-1f1e6 1f1ea-1f1e8 1f1ea-1f1ea 1f1ea-1f1ec 1f1ea-1f1ed 1f1ea-1f1f7 1f1ea-1f1f8 1f1ea-1f1f9 1f1ea-1f1fa 1f1eb-1f1ee 1f1eb-1f1ef 1f1eb-1f1f0 1f1eb-1f1f2 1f1eb-1f1f4 1f1eb-1f1f7 1f1ec-1f1e6 1f1ec-1f1e7 1f1ec-1f1e9 1f1ec-1f1ea 1f1ec-1f1eb 1f1ec-1f1ec 1f1ec-1f1ed 1f1ec-1f1ee 1f1ec-1f1f1 1f1ec-1f1f2 1f1ec-1f1f3 1f1ec-1f1f5 1f1ec-1f1f6 1f1ec-1f1f7 1f1ec-1f1f8 1f1ec-1f1f9 1f1ec-1f1fa 1f1ec-1f1fc 1f1ec-1f1fe 1f1ed-1f1f0 1f1ed-1f1f2 1f1ed-1f1f3 1f1ed-1f1f7 1f1ed-1f1f9 1f1ed-1f1fa 1f1ee-1f1e8 1f1ee-1f1e9 1f1ee-1f1ea 1f1ee-1f1f1 1f1ee-1f1f2 1f1ee-1f1f3 1f1ee-1f1f4 1f1ee-1f1f6 1f1ee-1f1f7 1f1ee-1f1f8 1f1ee-1f1f9 1f1ef-1f1ea 1f1ef-1f1f2 1f1ef-1f1f4 1f1ef-1f1f5 1f1f0-1f1ea 1f1f0-1f1ec 1f1f0-1f1ed 1f1f0-1f1ee 1f1f0-1f1f2 1f1f0-1f1f3 1f1f0-1f1f5 1f1f0-1f1f7 1f1f0-1f1fc 1f1f0-1f1fe 1f1f0-1f1ff 1f1f1-1f1e6 1f1f1-1f1e7 1f1f1-1f1e8 1f1f1-1f1ee 1f1f1-1f1f0 1f1f1-1f1f7 1f1f1-1f1f8 1f1f1-1f1f9 1f1f1-1f1fa 1f1f1-1f1fb 1f1f1-1f1fe 1f1f2-1f1e6 1f1f2-1f1e8 1f1f2-1f1e9 1f1f2-1f1ea 1f1f2-1f1eb 1f1f2-1f1ec 1f1f2-1f1ed 1f1f2-1f1f0 1f1f2-1f1f1 1f1f2-1f1f2 1f1f2-1f1f3 1f1f2-1f1f4 1f1f2-1f1f5 1f1f2-1f1f6 1f1f2-1f1f7 1f1f2-1f1f8 1f1f2-1f1f9 1f1f2-1f1fa 1f1f2-1f1fb 1f1f2-1f1fc 1f1f2-1f1fd 1f1f2-1f1fe 1f1f2-1f1ff 1f1f3-1f1e6 1f1f3-1f1e8 1f1f3-1f1ea 1f1f3-1f1eb 1f1f3-1f1ec 1f1f3-1f1ee 1f1f3-1f1f1 1f1f3-1f1f4 1f1f3-1f1f5 1f1f3-1f1f7 1f1f3-1f1fa 1f1f3-1f1ff 1f1f4-1f1f2 1f1f5-1f1e6 1f1f5-1f1ea 1f1f5-1f1eb 1f1f5-1f1ec 1f1f5-1f1ed 1f1f5-1f1f0 1f1f5-1f1f1 1f1f5-1f1f2 1f1f5-1f1f3 1f1f5-1f1f7 1f1f5-1f1f8 1f1f5-1f1f9 1f1f5-1f1fc 1f1f5-1f1fe 1f1f6-1f1e6 1f1f7-1f1ea 1f1f7-1f1f4 1f1f7-1f1f8 1f1f7-1f1fa 1f1f7-1f1fc 1f1f8-1f1e6 1f1f8-1f1e7 1f1f8-1f1e8 1f1f8-1f1e9 1f1f8-1f1ea 1f1f8-1f1ec 1f1f8-1f1ed 1f1f8-1f1ee 1f1f8-1f1ef 1f1f8-1f1f0 1f1f8-1f1f1 1f1f8-1f1f2 1f1f8-1f1f3 1f1f8-1f1f4 1f1f8-1f1f7 1f1f8-1f1f8 1f1f8-1f1f9 1f1f8-1f1fb 1f1f8-1f1fd 1f1f8-1f1fe 1f1f8-1f1ff 1f1f9-1f1e6 1f1f9-1f1e8 1f1f9-1f1e9 1f1f9-1f1eb 1f1f9-1f1ec 1f1f9-1f1ed 1f1f9-1f1ef 1f1f9-1f1f0 1f1f9-1f1f1 1f1f9-1f1f2 1f1f9-1f1f3 1f1f9-1f1f4 1f1f9-1f1f7 1f1f9-1f1f9 1f1f9-1f1fb 1f1f9-1f1fc 1f1f9-1f1ff 1f1fa-1f1e6 1f1fa-1f1ec 1f1fa-1f1f2 1f1fa-1f1f3 1f1fa-1f1f8 1f1fa-1f1fe 1f1fa-1f1ff 1f1fb-1f1e6 1f1fb-1f1e8 1f1fb-1f1ea 1f1fb-1f1ec 1f1fb-1f1ee 1f1fb-1f1f3 1f1fb-1f1fa 1f1fc-1f1eb 1f1fc-1f1f8 1f1fd-1f1f0 1f1fe-1f1ea 1f1fe-1f1f9 1f1ff-1f1e6 1f1ff-1f1f2 1f1ff-1f1fc 1f38c 1f3c1 1f3f3-1f308 1f3f3 1f3f4-2620 1f3f4-e0067-e0062-e0065-e006e-e0067-e007f 1f3f4-e0067-e0062-e0073-e0063-e0074-e007f 1f3f4-e0067-e0062-e0077-e006c-e0073-e007f 1f3f4 1f6a9".split(" ")
      }
    };


// --------------------- Chat basic operations --------- START -----------------

  Drupal.chat_nodejs=Drupal.chat_nodejs||{};
  Drupal.chat_nodejs.Cookie={};

  // Chat construction ---------------------------------------------------------

  Drupal.behaviors.chatConstruct = {

    attach:function(context,settings) {

      // Adjust panel height ---------------------------------------------------

      $.fn.adjustPanel=function(){

        $(this).find("ul, .subpanel").css({height:"auto"});     // Reset sub-panel and ul height

        var windowHeight=$(window).height(),                                   // Get the height of the browser viewport
          panelsub=$(this).find(".subpanel").height(),                 // Get the height of sub-panel
          panelAdjust=windowHeight-100,                                        // Viewport height - 100px (Sets max height of sub-panel)
          ulAdjust=panelAdjust-25;                                             // Calculate ul size after adjusting sub-panel

        // If sub-panel is taller than max height...
        if (panelsub>panelAdjust) {
          $(this).find(".subpanel").css({height:panelAdjust});  // Adjust sub-panel to max height
          $(this).find("ul").css({height:panelAdjust-48})       // Adjust subpanel ul to new size
        } else {
          // If sub-panel is smaller than max height...
          $(this).find("ul").css({height:"auto"})               // Set sub-panel ul to auto (default size)
        }

      };

      // Execute function on load
      $("#chatpanel").adjustPanel();                                           // Run the adjustPanel function on #chatpanel

      // Each time the viewport is adjusted/resized, execute the function
      $(window).resize(function(){
        $("#chatpanel").adjustPanel()
      });

    }

  };

  // Chat operations -----------------------------------------------------------

  Drupal.behaviors.chatOperations = {

    attach:function(context,settings) {

      // Read the Cookie to instantiate the previous state of windows. START ---
      if (Drupal.chat_nodejs.Cookie.read("chat_open_chat_uids")!==null) {

        var buddy_uids=[],
          temp_open_chat_uids=Drupal.chat_nodejs.Cookie.read("chat_open_chat_uids").split(","),
          array_intersect;

        $("#buddylist li a").each(function(){

          buddy_uids.push($(this).attr("class"))

        });

        array_intersect=arrayIntersect(temp_open_chat_uids,buddy_uids);

        if(Drupal.chat_nodejs.Cookie.read("chat_nodejs_login")&&array_intersect.length!==temp_open_chat_uids.length){

          Drupal.chat_nodejs.Cookie.delete("chat_open_chat_uids");
          Drupal.chat_nodejs.Cookie.delete("chat_current_open_chat_window_ids");
          Drupal.chat_nodejs.Cookie.delete("chat_nodejs_login")

        }else{

          if(Drupal.chat_nodejs.Cookie.read("chat_nodejs_login")){
            Drupal.chat_nodejs.Cookie.delete("chat_nodejs_login")
          }

          for(var e1 in temp_open_chat_uids){
            chat.open_chat_uids[temp_open_chat_uids[e1]]=temp_open_chat_uids[e1]
          }

        }

      }

      if(Drupal.chat_nodejs.Cookie.read("chat_current_open_chat_window_ids")!==null){

        var temp_open_chat_window_uids=Drupal.chat_nodejs.Cookie.read("chat_current_open_chat_window_ids").split(",");

        for(var e2 in temp_open_chat_window_uids){
          chat.current_open_chat_window_ids[temp_open_chat_window_uids[e2]]=temp_open_chat_window_uids[e2]
        }

      }
      // Read the Cookie to instantiate the previous state of windows. END -----

      // Click event on subpanels START ----------------------------------------
      // Closes chat box of the chosen user if it was opened
      bodyTag.on("click",".chatbox .subpanel_title span.to-close",function(e){

        var userId=$(this).closest(".chatbox").attr("id").split("_")[1];

        closeChatBox(userId);
        e.stopPropagation()

      });

      // Minimize the chat box
      bodyTag.on("click",".subpanel .subpanel_title span.min",function(){

        var local=$(this).closest(".chatbox"),
          maximize=$('<span class="glyphicon glyph-caret-up max" title="Maximize"></span>');

        $(this).closest(".subpanel").hide().siblings("div").hide(); //hide subpanel
        $(this).closest("li").children("a").removeClass("active");

        var $title_copy_local=$(this).closest(".subpanel").find(".subpanel_title").detach();

        $title_copy_local.find("span.min").remove().end().addClass("unactive").find("span.to-close").after(maximize);
        local.css({"background-color":"transparent","border-top":"unset"}).find("a.chatboxhead").before($title_copy_local);

        updateLocalChatWindowList(local.attr("id").split("_")[1],"min")

      });

      // Maximize the chat box
      bodyTag.on("click",".subpanel_title.unactive span.max",function(){

        var local=$(this).closest(".chatbox"),
          minimize=$('<span title = "Minimize" class="min">_</span>');

        $(this).parent().siblings("div").not(".emojionepicker").toggle(); //Toggle the subpanel and options to make active
        $(this).parent().siblings("a").removeClass("active"); //Remove active class on all subpanel trigger
        $(this).parent().siblings("a").toggleClass("active"); //Toggle the active class on the subpanel trigger
        $(this).closest(".chatbox").removeAttr("style").css({display:"list-item"});

        var title_copy_local=$(this).closest(".chatbox").find(".subpanel_title").detach();

        title_copy_local.find("span.max").remove().end().removeClass("unactive").find("span.to-close").after(minimize);
        local.find(".subpanel").prepend(title_copy_local.css("display","block"));

        // Chat box functions
        var isTextarea=local.find(".subpanel").children(".chatboxinput").children(".chatboxtextarea");

        if(isTextarea.length>0){

          isTextarea[0].focus();
          $(this).next(".subpanel").children(".chatboxcontent").scrollTop(local.find(".subpanel").children(".chatboxcontent")[0].scrollHeight)

        }

        updateLocalChatWindowList(local.attr("id").split("_")[1],"open")

      });

      // Create chat with the chosen user (Side of the sender)
      bodyTag.on("click","#chatpanel #buddylist li a",function(){

        var id=$(this).attr("class"),
          chatBoxTitle=$("#chatbox_"+id);

        if(!chatBoxTitle.length){

          chatWith(id,$(this).text());
          // Show chat history with the chosen user
          getThreadHistory()

        }else if(chatBoxTitle.css("display")==="none"){

          chatBoxTitle.css("display","block");
          chatBoxTitle.find(".chatboxtextarea").focus()

        }
        return false
      });
      // Click events on subpanels END -----------------------------------------

      // User chat status START ------------------------------------------------
      // User switch his chat to busy
      switch_user_status("status-2","status-1");
      // User switch his chat to online
      switch_user_status("status-1","status-2");

      // Switch user status function
      function switch_user_status(status1,status2){

        bodyTag.on("click",".tooltipster-opts #tooltip_content li."+status1,function(){

          var status=status1.split("-")[1];

          $(".chat_options ."+status2).removeClass(status2).addClass("chat_loading");

          $.post(drupalSettings.chat_nodejs["statusUrl"],{status:status},function(){
            $(".chat_options .chat_loading").removeClass("chat_loading").addClass(status1)
          })

        })

      }
      // User chat status END --------------------------------------------------

      // Get thread history
      getThreadHistory();

      // Message sending -------------------------------------------------------
      Drupal.chat_nodejs.sendMessages=function(){

        $.post(drupalSettings.chat_nodejs["sendUrl"],{
          chat_message_id:chat.send_current_message_id,
          chat_uid2:chat.send_current_uid2,
          chat_message:chat.send_current_message,
          form_id:drupalSettings.chat_nodejs["formId"],
          form_token:drupalSettings.chat_nodejs["formToken"]
        })

      };

      // Message sanitizing ----------------------------------------------------
      Drupal.chat_nodejs.parseHTML=function(text){

        var patt=/\b(http:\/\/|https:\/\/|ftp:\/\/|file:\/\/)?(www\.|ftp\.)?[A-Za-z0-9]+[-A-Z0-9@\/_$.]*(\.ac|\.ad|\.ae|\.aero|\.af|\.ag|\.ai|\.al|\.am|\.an|\.ao|\.aq|\.ar|\.arpa|\.as|\.asia|\.at|\.au|\.aw|\.ax|\.az|\.ba|\.bb|\.bd|\.be|\.bf|\.bg|\.bh|\.bi|\.biz|\.bj|\.bm|\.bn|\.bo|\.br|\.bs|\.bt|\.bv|\.bw|\.by|\.bz|\.ca|\.cat|\.cc|\.cd|\.cf|\.cg|\.ch|\.ci|\.ck|\.cl|\.cm|\.cn|\.co|\.com|\.coop|\.cr|\.cu|\.cv|\.cw|\.cx|\.cy|\.cz|\.de|\.dj|\.dk|\.dm|\.do|\.dz|\.ec|\.edu|\.ee|\.eg|\.er|\.es|\.et|\.eu|\.fi|\.fj|\.fk|\.fm|\.fo|\.fr|\.ga|\.gb|\.gd|\.ge|\.gf|\.gg|\.gh|\.gi|\.gl|\.gm|\.gn|\.gov|\.gp|\.gq|\.gr|\.gs|\.gt|\.gu|\.gw|\.gy|\.hk|\.hm|\.hn|\.hr|\.ht|\.hu|\.id|\.ie|\.il|\.im|\.in|\.info|\.int|\.io|\.iq|\.ir|\.is|\.it|\.je|\.jm|\.jo|\.jobs|\.jp|\.ke|\.kg|\.kh|\.ki|\.km|\.kn|\.kp|\.kr|\.kw|\.ky|\.kz|\.la|\.lb|\.lc|\.li|\.lk|\.lr|\.ls|\.lt|\.lu|\.lv|\.ly|\.ma|\.mc|\.md|\.me|\.mg|\.mh|\.mil|\.mk|\.ml|\.mm|\.mn|\.mo|\.mobi|\.mp|\.mq|\.mr|\.ms|\.mt|\.mu|\.museum|\.mv|\.mw|\.mx|\.my|\.mz|\.na|\.name|\.nc|\.ne|\.net|\.nf|\.ng|\.ni|\.nl|\.no|\.np|\.nr|\.nu|\.nz|\.om|\.org|\.pa|\.pe|\.pf|\.pg|\.ph|\.pk|\.pl|\.pm|\.pn|\.pr|\.pro|\.ps|\.pt|\.pw|\.py|\.qa|\.re|\.ro|\.rs|\.ru|\.rw|\.sa|\.sb|\.sc|\.sd|\.se|\.sg|\.sh|\.si|\.sj|\.sk|\.sl|\.sm|\.sn|\.so|\.sr|\.st|\.su|\.sv|\.sx|\.sy|\.sz|\.tc|\.td|\.tel|\.tf|\.tg|\.th|\.tj|\.tk|\.tl|\.tm|\.tn|\.to|\.tp|\.tr|\.travel|\.tt|\.tv|\.tw|\.tz|\.ua|\.ug|\.uk|\.us|\.uy|\.uz|\.va|\.vc|\.ve|\.vg|\.vi|\.vn|\.vu|\.wf|\.ws|\.xn|\.xxx|\.ye|\.yt|\.za|\.zm|\.zw)([-._~:\/?#\[\]@!$&'\(\)\*\+,;=][A-Za-z0-9\.\/\+&@#%=~_|]*)*\b/gi,
          a=text.match(patt),
          b=text.match(/\{{2}[a-z]+,[a-f0-9-]+\}{2}/g),
          pos=0,
          final_string="",
          prefix="";

        if(a){

          for(var i=0;i<a.length;i++){

            pos=text.indexOf(a[i])+a[i].length;
            prefix="";

            if(pos>text.length){

              pos=text.length
            }

            if(!(a[i].indexOf("http://")===0||a[i].indexOf("https://")===0)){

              prefix="http://"

            }

            final_string=final_string+text.substring(0,pos).replace(a[i],"<a target = '_blank' href='"+prefix+a[i]+"'>"+a[i]+"</a>");
            text=text.substring(pos,text.length)

          }

          if(text.length!==0){

            final_string=final_string+text

          }

          return position_smile(b,final_string)||final_string

        }

        return position_smile(b,text)||text

      };

      // Prepare message for sending and displaying ----------------------------
      Drupal.chat_nodejs.checkChatBoxInputKey=function(event,chatboxtextarea,chatboxtitle){

        var adjustedHeight,
          maxHeight=94;

        if(event.keyCode===13&&!event.shiftKey||event.type==="click"){

          if(event.type==="click"){chatboxtextarea=$(chatboxtextarea).closest("li").find(".chatboxtextarea")}
          // Play sound when message sent by sender
          if(drupalSettings.chat_nodejs["notificationSound"]==1){Drupal.chat_nodejs.sound_loader().playSound(drupalSettings.chat_nodejs["sound_message_sent"])}

          var message=$(chatboxtextarea).val(),
            currentTime=new Date,
            last_message_time;

          // Prevent sending a message if there are only whitespaces or(and) carriage returns
          message=message.replace(/(\r\n|\r|\n)/g,"{line-break}");
          message=message.replace(/^\s+/g,"");
          message=message.replace(/</g,"&lt;").replace(/>/g,"&gt;").replace(/\"/g,"&quot;");
          message=message.substr(0,255);

          $(chatboxtextarea).val("");
          $(chatboxtextarea).focus();
          $(chatboxtextarea).css("height","44px");

          if(message!==""){

            last_message_time=currentTime.getTime();
            chat.send_current_uid2=chatboxtitle;
            chat.send_current_message=message;
            chat.send_current_message_id="m_"+drupalSettings.chat_nodejs.uid+"_"+chat.send_current_uid2+"_"+last_message_time;

            Drupal.chat_nodejs.sendMessages();

          }
          return false
        }

        adjustedHeight=chatboxtextarea.clientHeight;

        if(maxHeight>adjustedHeight){

          adjustedHeight=Math.max(chatboxtextarea.scrollHeight,adjustedHeight);

          if(maxHeight){
            adjustedHeight=Math.min(maxHeight,adjustedHeight)
          }

          if(adjustedHeight>chatboxtextarea.clientHeight){
            $(chatboxtextarea).css("height",adjustedHeight+8+"px")
          }

        }else{
          $(chatboxtextarea).css("overflow","auto")
        }

        return true

      };

      // User current status ---------------------------------------------------
      Drupal.chat_nodejs.changeStatus=function(id,state) {

        if(state===0){

          $("#"+id+" .subpanel_title > div").removeClass("status-1").removeClass("status-2").addClass("status-0");
          $("#"+id+" .chat_userOffline").css("display","block")

        }else if(state===1){

          $("#"+id+" .subpanel_title > div").removeClass("status-0").removeClass("status-2").addClass("status-1");
          $("#"+id+" .chat_userOffline").css("display","none")

        }else if(state===2){

          $("#"+id+" .subpanel_title > div").removeClass("status-0").removeClass("status-1").addClass("status-2");
          $("#"+id+" .chat_userOffline").css("display","none")

        }

      };

      // Sound notifications ---------------------------------------------------
      Drupal.chat_nodejs.sound_loader=function(){

        return{

          playSound:function(){
            return $('<audio class="sound-player" autoplay="autoplay" style="display:none;">'+
              '<source src="'+arguments[0]+'" />'+
              '<embed src="'+arguments[0]+'" hidden="true" autostart="true" loop="false"/>'+
              "</audio>").appendTo("body")
          },

          stopSound:function(){
            $(".sound-player").remove()
          }

        }

      }

    }

  };

  // ------------------------- Chat tooltips ------------- START ---------------

  // Main chat window options
  bodyTag.on("click tap","#chat #chatpanel .subpanel_title .glyph-cog span.clicked:not(.tooltipstered)",function(){

    var offset_=$(this).closest("#chatpanel").offset(),
      left=offset_.left,
      right=$(window).width()-left-$(this).closest("#chatpanel").width();

    $(this).tooltipster({

      debug:false,
      theme:["tooltipster-opts"],
      content:$("#tooltip_content"),
      interactive:true,
      trigger:"custom",
      triggerOpen:{
        click:true,
        tap:true
      },
      triggerClose:{
        click:true,
        tap:true
      },
      minWidth:$("#chatpanel").width(),

      functionPosition:function(instance,helper,position){

        position.coord.left-=right;

        return position

      },

      functionInit:function(instance,helper){

        var instance_content = $(instance.__Content);

        // Manipulate content of the tooltip if more than one window has been openned (all windows except of current)
        // Current window manipulated from the main nodeJS thread handling
        Drupal.chat_nodejs.processUserSwitchedSound=function(sound_state){
          instance_content.find("#chat-chat-options-s").html('<a href="#">'+sound_state.action+'</a><span class="glyphicon glyph-volume-'+sound_state.sign+'"></span>');
          // Save current state of the sound notifications to reuse without the page reload.
          // Do not delete!!!
          // This raw is required when more then 1 window tab is openned
          drupalSettings.chat_nodejs["notificationSound"]=sound_state.command
        };

        instance_content.css("display","block")

      },

      functionReady:function(instance,helper){

        $(helper.tooltip).on("click","span.to-close",function(){

          $(helper.tooltip).hide();
          $(helper.origin).trigger("click")

        })

      }

    });

    $(this).trigger("click")

  });

  // Delete own message from the chat with another user
  jChat.on("click tap",".chatboxcontent span.clicked:not(.tooltipstered)",function(){

    var right=$(this).closest(".chatboxcontent").width()/2-15;

    $(this).tooltipster({

      debug:false,
      theme:["tooltipster-opts"],
      content:'<div id="tooltip_edit">' +
        '<span>Edit</span>' +
        '<span class="to-close" title="Close">x</span>' +
        '<ul>' +
        '<li class="hr"><hr /></li>' +
        '<li class="delete '+$(this).closest("p").attr("class").split(" ")[0]+'">' +
        '<a>Delete</a>' +
        '<span class="glyphicon glyph-trash" style="background:url(' + drupalSettings.chat_nodejs['trashImgPath'] + ') no-repeat center;width:15px;height:15px;"></span>' +
        '</li>' +
        '</ul>' +
        '</div>',
      contentAsHTML:true,
      interactive:true,
      trigger:"custom",
      triggerOpen:{
        click:true,
        tap:true
      },
      triggerClose:{
        click:true,
        tap:true
      },
      minWidth:$(".chatboxcontent").width(),

      functionPosition:function(instance,helper,position){

        position.coord.left+=right;

        return position

      },

      functionReady:function(instance,helper){

        $(helper.tooltip).on("click","span.to-close, li.delete",function(){

          var _tooltip=$(helper.tooltip),
            message_id=_tooltip.find("li.delete").attr("class").split(" ")[1],
            receiver_id=message_id.split("_")[2],
            sender_id=message_id.split("_")[1];

          _tooltip.hide();
          $(helper.origin).trigger("click");

          if($(this).hasClass("delete")){

            var message={messageBody:{
                receiver_id:receiver_id,
                sender_id:sender_id
              }
            };

            $.post(drupalSettings.chat_nodejs["editMessage"],{delete_msg:message_id, delete_msg_data:JSON.stringify(message)})

          }

        })

      }

    });

    $(this).trigger("click")

  });

  // Informational Tooltip shows "title" data of the element on "mouseenter" event
  bodyTag.on("mouseenter","#chat .chatbox .subpanel_title:not(.unactive) span:not(.tooltipstered), #chat .chatbox .subpanel_title.unactive span:not(.tooltipstered), #chat .chatbox div.emojionepicker-picker:not(.tooltipstered), #chat #chatpanel .subpanel_title span.options:not(.tooltipstered), .tooltipster-opts #tooltip_content span.to-close:not(.tooltipstered), #chat .chatboxcontent p, #chat .chatboxcontent p span.options-own, #tooltip_edit span.to-close:not(.tooltipstered), #tooltip_handle span.to-close:not(.tooltipstered), #chat-wrapper .subpanel .subpanel_title span.chat-close:not(.tooltipstered), #call-messenger.toggle-tooltip:not(.tooltipstered)",function(){

    $(this).tooltipster({debug:false,theme:["tooltipster-opts"],zIndex:999999}).tooltipster("open");

    // Show edit options for messages of the currently logged in user on mouseenter
    if($(this).closest(".chatboxcontent")&&!$(this).hasClass("deleted")){

      if($(this).prev("div").find("a").text()===$("#chatpanel .chat_options a.name").text()&&!$(this).children().hasClass("options-own")){

        $('<span class="options-own glyphicon glyph-opt-vert" title="Edit"><span class="clicked"></span></span>').hide().prependTo($(this)).fadeIn("slow")

      }

    }

    if($(this).hasClass("options-own")){
      stop_hide_delay()
    }

  });

  bodyTag.on("mouseleave","#chat .chatboxcontent p",function(){

    // Hide edit options for messages of the currently logged in user on mouseleave
    var opts=$(this).children(".options-own");

    if(opts){
      start_hide_delay(opts,$(this))
    }

  });

  // Tooltip shows friend data when click on it's small image in buddylist
  bodyTag.on("click tap","#chat #chatpanel .subpanel  #buddylist li img:not(.tooltipstered)",function(){

    $(this).tooltipster({

      debug:false,
      theme:["tooltipster-opts"],
      content:"",
      contentAsHTML:true,
      interactive:true,
      trigger:"custom",
      triggerOpen:{
        click:true,
        tap:true
      },
      triggerClose:{
        click:true,
        tap:true
      },
      minWidth:$("#chatpanel").width(),

      functionBefore:function(instance,helper){

        var that=$(helper.origin).closest("li"),
          src=that.find("img").attr("src"),
          uid=that.find("a").attr("class"),
          name=that.find("a").text();

        // Close previously opened tooltips
        $.each($.tooltipster.instances(),function(i,instance){
          instance.close()
        });

        instance.content('<div id="tooltip_user_details" style="font-size: 13px;">'+
          '<div class="col-md-12" style="padding:0;">'+
          '<p class="user-name" style="text-align: center;border-bottom:1px solid #8888;margin: 1em 0;padding-bottom: 1em;font-weight: bold;">'+
          '<a href="/user/'+uid+'" target="_blank" style="color:#222;">'+name+"</a>"+
          "</p>"+
          '<div style="text-align:center;margin:1em 0;">'+
          '<a href="/user/'+uid+'" target="_blank">'+
          '<img alt="User profile '+name+'" src="'+src+'" width="170" height="170">'+
          "</a>"+
          "</div>"+
          "</div>"+
          "</div>");

      },

      functionFormat:function(instance,helper,content){return content}

    });

    $(this).trigger("click")

  });

  // Chat tooltips END ---------------------------------------------------------

  // Chat Emoji operations START -----------------------------------------------

  // Hide Emoji container on click anywhere outside of the element
  $(document).on("click",function(e){

    var jEmojiContainer=$("."+emojiContainer);
    if(jEmojiContainer.is(":visible")===false||$(e.target).is(".emojionepicker-picker, .emojione")){return}

    jEmojiContainer.fadeOut()

  });

  // Create Emoji container and load section header
  function createContainer(el){

    $('<div class="'+emojiContainer+'"><nav></nav></div>').insertAfter(el);

    var navItems="";

    $.each(emojiList,function(k,v){

      var emoji='<span class="emojione-24-'+k+" _"+v["header"]+'"></span>';

      navItems+='<div class="tab'+(navItems===""?" active":"")+'" data-tab="'+k+'" onclick="">'+emoji+"</div>"

    });

    $("."+emojiContainer+" nav").append(navItems);
    $("."+emojiContainer+" nav .tab").click(function(e){

      loadEmojies($(this).attr("data-tab"));
      e.stopPropagation()

    });

    // Emoji click event
    $(document).off("click","."+emojiContainer+" section span");
    $(document).on("click","."+emojiContainer+" section span",function(){

      activeEl=$(this).closest("li").find(".chatboxtextarea");

      var caretPos=activeEl.selectionStart,
        textAreaTxt=activeEl.val(),
        type_of_smile,src_1,src_2,smile,txtToAdd;

      smile=$(this).attr("class").split(" _");
      type_of_smile=smile[0].split("-");
      src_1=type_of_smile[type_of_smile.length-1];
      src_2=smile[1];
      txtToAdd="{{"+src_1+","+src_2+"}}";
      activeEl.val(textAreaTxt.substring(0,caretPos)+txtToAdd);
      activeEl.focus();
      activeEl.selectionStart=caretPos+txtToAdd.length;
      activeEl.selectionEnd=caretPos+txtToAdd.length

    })

  }

  // Generate/Change Emoji section
  function loadEmojies(section){

    var sectionHtml="";

    if($("."+emojiContainer+" section."+section).length>0){

      if($("."+emojiContainer+" .tab[data-tab='"+section+"']").hasClass("active")){return}

      $("."+emojiContainer+" section").fadeOut().promise().done(function(){

        $(this).hide();
        $("."+emojiContainer+" section."+section).fadeIn();
        $("."+emojiContainer+" .tab").removeClass("active");
        $("."+emojiContainer+" .tab[data-tab='"+section+"']").addClass("active")

      });

      return

    }

    if(typeof emojiList[section]==="undefined"){return}

    $.each(emojiList[section].content,function(k,v){

      sectionHtml+='<span class="emojione-24-'+section+" _"+v+'"></span>'

    });

    $('<section class="'+section+'">'+sectionHtml+"</section>").insertAfter("."+emojiContainer+" nav");
    $("."+emojiContainer+" section").fadeOut().promise().done(function(){

      $("."+emojiContainer+" section."+section).fadeIn();
      $("."+emojiContainer+" .tab").removeClass("active");
      $("."+emojiContainer+" .tab[data-tab='"+section+"']").addClass("active")

    })

  }

  // Chat Emoji operations END -------------------------------------------------

  // Chat Cookies handling START -----------------------------------------------

  /**
   * Create cookie
   */
  Drupal.chat_nodejs.Cookie.create=function(name,value,minutes,domain){

    var expires="",
      t_domain="",
      date=new Date;

    if(typeof minutes!=="undefined"){
      date.setTime(date.getTime()+minutes*60*1e3);
      expires="; expires="+date.toUTCString()
    }else{
      date.setTime(date.getTime()+30*24*60*60*1e3);
      expires="; expires="+date.toUTCString()
    }

    if(typeof domain!=="undefined"){
      t_domain="; domain=."+domain
    }

    document.cookie=name+"="+value+expires+t_domain+"; path=/; SameSite=Strict"

  };

  /**
   * Read cookie
   */
  Drupal.chat_nodejs.Cookie.read=function(name){

    var nameEQ=name+"=",
      ca=document.cookie.split(";");

    for(var i=0;i<ca.length;i++){
      var c=ca[i];
      while(c.charAt(0)===" "){
        c=c.substring(1,c.length)
      }
      if(c.indexOf(nameEQ)===0){
        return c.substring(nameEQ.length,c.length)
      }
    }

    return null

  };

  /**
   * Delete cookie
   */
  Drupal.chat_nodejs.Cookie.delete=function(name){

    Drupal.chat_nodejs.Cookie.create(name,"",-1)

  };

  // Chat Cookies handling END -------------------------------------------------

// ------------- Chat functions ------------------------------------------------
// -----------------------------------------------------------------------------

  function start_hide_delay(element){

    hide_delay=setTimeout(function(){
      element.fadeOut("slow",function(){
        element.remove()
      })
    },5e3)

  }

  function stop_hide_delay(){
    clearTimeout(hide_delay)
  }

  // Sender side.
  // Open chat with the chosen user.
  function chatWith(chatboxtitle,chatboxname){

    createChatBox(chatboxtitle,chatboxname);
    $("#chatbox_"+chatboxtitle+" a:first").click();                            // Toggle subpanel to make active
    $("#chatbox_"+chatboxtitle+" .chatboxtextarea").focus();

    if(!(chatboxtitle in chat.open_chat_uids)&&!(chatboxtitle in chat.current_open_chat_window_ids)){
      updateLocalChatWindowList(chatboxtitle,"open")
    }

  }

  // Create Chat box
  function createChatBox(chatboxtitle,chatboxname,chatboxblink){

    var jChatboxTitle=$("#chatbox_"+chatboxtitle),
      jChatboxTitleTextArea=$("#chatbox_"+chatboxtitle+" .chatboxtextarea"),
      isMinimized=false,
      smiles;

    chatboxname=chatboxname&&chatboxname.length>14?chatboxname.substring(0,13)+"...":chatboxname;

    if(jChatboxTitle.length){

      if(chatboxtitle in chat.open_chat_uids&&chatboxtitle in chat.current_open_chat_window_ids){

        if(jChatboxTitle.css("display")==="none"){
          jChatboxTitle.css("display","block")
        }

        jChatboxTitleTextArea.focus();
        return
      }
    }

    if(chatboxtitle in chat.open_chat_uids&&!(chatboxtitle in chat.current_open_chat_window_ids)){
      isMinimized=true
    }

    var picker=$('<div class="emojionepicker-picker" title="Select Smiles" data-index="0" style="top: 5px; left: 5px; display: '+(isMinimized?"none;":"block;")+'"></div>'),
      send=$('<div class = "send-button" onclick = "return Drupal.chat_nodejs.checkChatBoxInputKey( event, this, \''+chatboxtitle+"' );\""+(isMinimized?'style="display: none;"':"")+'><span class="glyphicon glyph-send"></span> Send</div>'),
      subpanel_title='<div class="subpanel_title '+(isMinimized?"unactive":"")+'">'+
        '<span class="'+chatboxtitle+' to-close" title="Close">x</span>'+
        (isMinimized?'<span title = "Maximize" class="glyphicon glyph-caret-up max"></span>':'<span title = "Minimize" class="min">_</span>')+
        '<div class="status-0"></div>'+chatboxname+
        "</div>";

    $(" <li />").attr("id","chatbox_"+chatboxtitle)
      .addClass("chatbox")
      .css({display:isMinimized?"block":"none","background-color":isMinimized?"transparent":"","border-top":isMinimized?"unset":""})
      .html((isMinimized?subpanel_title:"")+
        '<a class="chatboxhead"></a>'+
        '<div class="subpanel" '+(isMinimized?'style="display: none;"':"")+">"+(!isMinimized?subpanel_title:"")+
        '<div class="chatboxcontent" style="position:relative">'+'' +
        '<div class="chatbox-history-0"></div>'+
        "</div>"+
        '<div class="chat_userOffline" style="display: none">'+chatboxname+" is currently offline.</div>"+
        '<div class="chatboxinput" style="position: relative;">'+
        '<textarea placeholder="Enter your message..." class="chatboxtextarea" onkeydown="return Drupal.chat_nodejs.checkChatBoxInputKey(event,this,\''+chatboxtitle+"');\"></textarea>"+
        "</div>"+
        "</div>").append(picker).on("click",".emojionepicker-picker",function(){

      if($("."+emojiContainer).length===0){
        createContainer(this)
      }

      // Emoji picker click event
      if($("."+emojiContainer).is(":visible")){$("."+emojiContainer).fadeOut();return}

      // Load Emojies section
      loadEmojies($("."+emojiContainer+" nav div.active").attr("data-tab"));

      $("."+emojiContainer).css({bottom:"35px",left:0}).fadeIn();

      if($(this).parent().find("."+emojiContainer).length===0){
        smiles=$("."+emojiContainer).detach();
        $(this).parent().append(smiles)
      }

    }).append(send).prependTo($("#mainpanel"));

    if(chatboxblink===1){
      $("#chatbox_"+chatboxtitle+" .chatboxhead").addClass("chatboxblink")
    }

    jChatboxTitleTextArea.blur(function(){

      jChatboxTitleTextArea.removeClass("chatboxtextareaselected")

    }).focus(function(){

      $("#chatbox_"+chatboxtitle+" .chatboxhead").removeClass("chatboxblink");jChatboxTitleTextArea.addClass("chatboxtextareaselected")

    });

    jChatboxTitle.click(function(){

      if($("#chatbox_"+chatboxtitle+" .chatboxcontent").css("display")!=="none"){

        jChatboxTitleTextArea.focus();
        updateLocalChatWindowList(chatboxtitle,"open")

      }

    });

    jChatboxTitle.show()

  }

  // Process Chat data
  function processChatData(data,history,history_id,chatbox_id){

    var chat_messages=data,
      h_id=history_id?history_id:0,
      history_data=history?" .chatbox-history-"+h_id:"",
      jChatLoading=$(".chat_options .chat_loading"),
      current_user_id=$("#chatpanel .chat_options a.name").attr("class").split(" ")[1];

    // User status switching
    switch(drupalSettings.chat_nodejs.status){

      case"1":

        jChatLoading.removeClass("chat_loading").addClass("status-1");
        break;

      case"2":

        jChatLoading.removeClass("chat_loading").addClass("status-2");
        break

    }

    // Delete message in opponents chat if it was deleted in owner's chat
    if(chat_messages["msg_delete"]){

      var msg_delete_arr=chat_messages["msg_delete"].split("_"),
        ownerId,receiverId;

      ownerId=msg_delete_arr[2];
      receiverId=msg_delete_arr[1];

      if($("#chatpanel .chat_options a.name").hasClass(ownerId)&&$("#chatbox_"+receiverId)){

        // Show "Message deleted..." in the opponent's window
        $("#chatbox_"+receiverId+" .chatboxcontent").find("p."+chat_messages["msg_delete"]).attr("class","deleted").text("Message deleted...").hide().fadeIn("slow");
        // Delete message from the helper DB
        $.get(drupalSettings.chat_nodejs["editMessage"],{msg_deleted:chat_messages["msg_delete"]})

      }

    }

    // Chat title notifications
    if(parseInt(chat_messages.status)===1&&$("body").hasClass("hidden_")){
      flashTitle(drupalSettings.chat_nodejs["newMessage"])
    }

    chat_messages.status=0;
    // Read the Cookie to check if they correspond
    // the currently existing users in friend list. START ----------------------
    if(Drupal.chat_nodejs.Cookie.read("chat_open_chat_uids")!==null){

      var open_chat_uids=Drupal.chat_nodejs.Cookie.read("chat_open_chat_uids").split(","),
        friend_uids=$("#chatpanel .subpanel .item-list li"),
        friend_uids_arr=[];

      friend_uids.each(function(index,element){
        friend_uids_arr.push($(element).find("a").attr("class"))
      });

      for(var i=0;i<open_chat_uids.length;i++){
        if($.inArray(open_chat_uids[i],friend_uids_arr)===-1){
          closeChatBox(open_chat_uids[i])
        }
      }

    }else{

      $('li[id^="chatbox_"]').each(function(){

        var id=this.id.split("_")[1];
        closeChatBox(id)

      })

    }
    // Read the Cookie to check if they correspond
    // the currently existing users in friend list. END ------------------------

    // Chat messages
    if(!chat_messages.status||chat_messages.status==0){
      // If there is a request to show more old messages
      if(history_id>0){
        $("#"+chatbox_id+" .chatboxcontent").prepend("<div class=chatbox-history-"+history_id+"></div>")
      }

      $.each(chat_messages.messages,function(index,value){

        var drupalselfmessage=value["uid1"]==drupalSettings.chat_nodejs.uid,
          chatboxtitle=drupalselfmessage?value["uid2"]:value["uid1"],
          currentTime=new Date,
          hours=currentTime.getHours(),
          minutes=currentTime.getMinutes(),
          formattedCurrentTime,
          jChatboxTitle=$("#chatbox_"+chatboxtitle),
          jChatboxTitleContent=$("#chatbox_"+chatboxtitle+" .chatboxcontent");

        hours=hours<10?"0"+hours:hours;
        minutes=minutes<10?"0"+minutes:minutes;
        formattedCurrentTime=hours+":"+minutes;
        formattedCurrentTime=typeof history==="boolean"&&history?value.timestamp:formattedCurrentTime;

        if(jChatboxTitle.length<=0){
          createChatBox(chatboxtitle,value.name,1)
        }else if($("#chatbox_"+chatboxtitle+" .subpanel").is(":hidden")){

          if(jChatboxTitle.css("display")==="none"){
            jChatboxTitle.css("display","block")
          }

          $("#chatbox_"+chatboxtitle+" .chatboxhead").addClass("chatboxblink");

          $("body").on("click","#chatbox_"+chatboxtitle+" .chatboxhead",function(){

            $("#chatbox_"+chatboxtitle+" .chatboxhead").removeClass("chatboxblink");
            chat.send_current_uid2=chatboxtitle

          });

          $("#chatbox_"+chatboxtitle+" .chatboxtextarea").focus()

        }

        if(value["uid1"]==drupalSettings.chat_nodejs.uid){
          value.name=drupalSettings.chat_nodejs.username
        }

        if($("."+value["message_id"])[0]){return}

        value.message=Drupal.chat_nodejs.parseHTML(value.message);
        // Assign the carriage returns in sender's history and receiver's window
        // if they are inside of the string
        value.message=value.message.replace(/{line-break}/g,"<br />");
        // Received messages (if more than one message without opponents answer)
        if($("#chatbox_"+chatboxtitle+" .chatboxcontent"+history_data+" .chatboxusername a:last").html()===value.name){
          message_with_user_details_history(value,formattedCurrentTime,chatboxtitle,history_data)
        }else{
          // Received messages (if one message received!!!)
          if($("div.chatbox-history-0").is(":empty")){
            getThreadHistory()
          }
          message_with_user_details_history(value,formattedCurrentTime,chatboxtitle,history_data)
        }

        if(!history_id&&jChatboxTitleContent[0]){
          jChatboxTitleContent.scrollTop(jChatboxTitleContent[0].scrollHeight)
        }
        if(history_id&&jChatboxTitleContent[0]){
          jChatboxTitleContent.scrollTop(jChatboxTitleContent[0].scrollHeight/2)
        }

      });

      // Helper function to create the new message in receivers window with the details.
      function message_with_user_details_history(value,formattedCurrentTime,chatboxtitle,history_data){

        var output='<div class="chatboxusername">',
          class_deleted=value.message==="Message deleted..."?"deleted ":"";

        output=output+'<div class="chatboxuserpicture"><img alt="" height="30" width="30" src="'+value.p+'" /></div>';
        output=output+'<span class="chatboxtime">'+formattedCurrentTime+'</span><a href="'+drupalSettings.path["baseUrl"]+"user/"+value["uid1"]+'">'+(value.name.length>14?value.name.substring(0,13)+"...":value.name)+'</a></div><p class="'+class_deleted+value["message_id"]+'">'+value.message+"</p>";

        $("#chatbox_"+chatboxtitle+" .chatboxcontent"+history_data).append(output);

        // Chat sound notifications (new message received)
        if(parseInt(drupalSettings.chat_nodejs["notificationSound"])===1&&!history_data&&parseInt(drupalSettings.chat_nodejs["chatClosed"])===0){
          var sound=Drupal.chat_nodejs.sound_loader();
          sound.playSound(drupalSettings.chat_nodejs["sound_new_message"])
        }

      }

      // Chat window with the chosen user
      // User status control Online/Offline START -------------------------------
      $('li[id^="chatbox_"]').each(function(){

        var user_online=$("#chatpanel .subpanel .item-list li a."+this.id.split("_")[1]).closest("li");

        if(user_online.hasClass("status-0")){
          Drupal.chat_nodejs.changeStatus(this.id,0)
        }else if(user_online.hasClass("status-1")){
          Drupal.chat_nodejs.changeStatus(this.id,1)
        }else if(user_online.hasClass("status-2")){
          Drupal.chat_nodejs.changeStatus(this.id,2)
        }

      });
      // User status control Online/Offline END --------------------------------

      // If element has ScrollBar START ----------------------------------------
      $.fn.hasScrollBar=function(){
        return this.get(0).scrollHeight>this.height()
      };

      if($("#chatpanel .subpanel .item-list ul").hasScrollBar()){
        $("#chat .subpanel .chat_options a.status").css("right","16px")
      }else{
        $("#chat .subpanel .chat_options a.status").css("right","0")
      }
      // If element has ScrollBar END ------------------------------------------

      // Buddy list construction START -----------------------------------------
      if(typeof chat_messages.buddylist!=="undefined"){

        var sublists_state={buddylist:""},
          sublists_state_key=chat_messages["sublists_state"].substring(1,chat_messages["sublists_state"].length);

        $.each(sublists_state,function(key){

          if(key===sublists_state_key){
            sublists_state[key]="in"
          }else{
            sublists_state[key]=""
          }

        });

        buddylist_update_timeout++;
        // Delay updates of the buddylist
        // Prevents fast hiding the options panel of the user details request
        if(buddylist_update_timeout>10){

          $("#chatpanel .subpanel .item-list ul").addClass(sublists_state["buddylist"]).empty();

          $.each(chat_messages.buddylist,function(key,value){

            if(key!=="total"){

              if(key!=drupalSettings.chat_nodejs.uid){

                var user_image=new Image(24,24),
                  link=$('<a class="'+key+'" >'+value.name+"</a>");

                user_image.src=value.p;
                $('<li class="status-'+value.status+'"></li>').append(user_image).append(link).appendTo("#chatpanel .subpanel .item-list ul");
                buddylist_update_timeout=0

              }

            }else{

              $("#chatpanel .online-count").html(value);
              // Show message "No contacts"
              // if there are no any users in friend list
              // (buddylist object has only one property - "total")
              if(value===0&&Object.keys(chat_messages.buddylist).length===1){
                $("#chatpanel .subpanel .item-list ul").append(drupalSettings.chat_nodejs["noUsers"])
              }

            }

          });
          $("#chatpanel .subpanel ul li:last-child").addClass("last");
          //Update Timestamp.
          chat.last_timestamp=chat_messages.last_timestamp
        }
      }
      // Buddy list construction END -------------------------------------------
    }

  }

  // Chat box close
  function closeChatBox(chatboxtitle){

    updateLocalChatWindowList(chatboxtitle,"close");
    $("#chatbox_"+chatboxtitle).css("display","none")

  }

  // Update local Chat window list
  function updateLocalChatWindowList(chatboxtitle,state){

    if(state==="min"){
      chat.open_chat_uids[chatboxtitle]=chatboxtitle;
      delete chat.current_open_chat_window_ids[chatboxtitle]
    }else if(state==="open"){
      chat.open_chat_uids[chatboxtitle]=chatboxtitle;
      chat.current_open_chat_window_ids[chatboxtitle]=chatboxtitle
    }else{
      delete chat.open_chat_uids[chatboxtitle];
      delete chat.current_open_chat_window_ids[chatboxtitle]
    }

    var temp_open_chat_uids=[];

    for(var e in chat.open_chat_uids){
      temp_open_chat_uids.push(chat.open_chat_uids[e])
    }

    if(temp_open_chat_uids.join(",")!==""){
      Drupal.chat_nodejs.Cookie.create("chat_open_chat_uids",temp_open_chat_uids.join(","))
    }else{
      Drupal.chat_nodejs.Cookie.delete("chat_open_chat_uids")
    }

    var temp_open_chat_window_uids=[];

    for(var e_w in chat.current_open_chat_window_ids){
      temp_open_chat_window_uids.push(chat.current_open_chat_window_ids[e_w])
    }

    if(temp_open_chat_window_uids.join(",")!==""){
      Drupal.chat_nodejs.Cookie.create("chat_current_open_chat_window_ids",temp_open_chat_window_uids.join(","))
    }else{
      Drupal.chat_nodejs.Cookie.delete("chat_current_open_chat_window_ids")
    }

  }

  // Get messaging history with the currenly chosen user
  function getThreadHistory(history_id,chatbox_id){

    if(drupalSettings.chat_nodejs&&Object.keys(chat.open_chat_uids).length){
      var temp_open_chat_uids=[];
      // Default value inside of function getThreadHistory( history_id = 0, chatbox_id )
      // will not work in Internet Explorer!!!
      history_id=history_id||0;
      for(var e in chat.open_chat_uids){
        temp_open_chat_uids.push(chat.open_chat_uids[e])
      }

      $.post(drupalSettings.chat_nodejs["threadHistoryUrl"],{chat_open_chat_uids:temp_open_chat_uids.join(","),history_id:history_id},function(data){

        var temp_open_ids={};
        for(var i in chat.current_open_chat_window_ids){
          temp_open_ids[i]=chat.current_open_chat_window_ids[i]
        }

        processChatData(data,true,history_id,chatbox_id);

        for(var j in temp_open_ids){
          chatWith(temp_open_ids[j],data["names"][j])
        }

      }).done(function(data){

        if(data.messages.length){
          old_history=true
        }

      })

    }

  }

  // Main chat window close
  bodyTag.on("click tap","#chat #chatpanel .subpanel_title .chat-close span.clicked",function(){

    $(this).closest("#chat-wrapper").fadeOut(2e3,"swing",function(){

      $("#call-messenger").fadeIn(2e3,"swing");
      $.post(drupalSettings.chat_nodejs["statusUrl"],{chat_closed:1});
      drupalSettings.chat_nodejs["chatClosed"]=1

    })

  });

  // Main chat window open
  bodyTag.on("click tap","#call-messenger",function(){

    $(this).fadeOut(2e3,"swing",function(){

      $("#chat-wrapper").fadeIn(2e3,"swing");
      $.post(drupalSettings.chat_nodejs["statusUrl"],{chat_closed:0});
      drupalSettings.chat_nodejs["chatClosed"]=0

    })

  });

  // Flash title on new message ------------------------------------------------
  window.flashTitle=function(msg,quantity){

    function step(){

      var jBody=$("body");

      document.title=document.title===original_title?msg:original_title;

      if(--quantity>0){
        if(quantity<2&&jBody.hasClass("hidden_")){
          quantity=10
        }
        timeout_flash_title=setTimeout(step,1e3);
        if(!jBody.hasClass("hidden_")){
          quantity=0;
          document.title=original_title;
          cancelFlashTitle(timeout_flash_title)
        }
      }

    }

    quantity=parseInt(quantity);
    if(isNaN(quantity)){quantity=10}

    cancelFlashTitle(timeout_flash_title);
    step()

  };

  window.cancelFlashTitle=function(){
    document.title=original_title;
    clearTimeout(timeout_flash_title)
  };

  // Emoji positioning in a final string before sending ------------------------
  function position_smile(b,final_string){

    var final_string_smile="",
      pos_smile=0,
      type,bj,j;

    if(b){

      for(j=0;j<b.length;j++){

        pos_smile=final_string.indexOf(b[j])+b[j].length;
        type=b[j].split(",")[0].slice(2);
        bj=b[j].split(",")[1].slice(0,-2);
        final_string_smile=final_string_smile+final_string.substring(0,pos_smile).replace(b[j],'<span class="emojione-24-'+type+" _"+bj+'"></span>');
        final_string=final_string.substring(pos_smile,final_string.length)

      }

      if(final_string.length!==0){

        final_string_smile=final_string_smile+final_string

      }

      return final_string_smile

    }

  }

  // Array intersect
  function arrayIntersect(a,b){
    return $.grep(a,function(i){return $.inArray(i,b)>-1})
  }

  // Add scrolls to chat content on mouseenter
  bodyTag.on("mouseenter","#chat .subpanel .chatboxcontent",function(){
    $(this).css("overflow-y","auto")
  });

  // Delete scrolls from chat content on mouseleave
  bodyTag.on("mouseleave","#chat .subpanel .chatboxcontent",function(){
    $(this).css("overflow-y","hidden")
  });

  // Add event listener on "scroll" to the document (only modern browsers (IE>8))
  // "Scroll" event can not be delegated with jQuery!!!
  document.addEventListener("scroll",function(event){

    var last_history_class,
      last_history_id,
      chatbox_id;

    if(event.target.className==="chatboxcontent"){
      // Load more messages from chat history when chat content window scrolled to top
      if(event.target.scrollTop<500&&event.target.scrollHeight-event.target.scrollTop>1200&&old_history){

        chatbox_id=event.target.offsetParent.offsetParent.getAttribute("id");
        last_history_class=event.target.children[0].getAttribute("class");
        last_history_id=parseInt(last_history_class.split("-")[2]);

        getThreadHistory(last_history_id+1,chatbox_id);
        // To prevent multiple calls of the function getThreadHistory for old messages
        // Allow to call it next time only after AJAX post request is finished
        old_history=false

      }
    }},true /*Capture event*/
  );

  // Detect if chat window is currently active START ---------------------------

  // Standards:
  if(hidden in document)
    document.addEventListener("visibilitychange",onchange);
  else if((hidden="mozHidden")in document)
    document.addEventListener("mozvisibilitychange",onchange);
  else if((hidden="webkitHidden")in document)
    document.addEventListener("webkitvisibilitychange",onchange);
  else if((hidden="msHidden")in document)
    document.addEventListener("msvisibilitychange",onchange);
  // IE 9 and lower:
  else if("onfocusin"in document)
    document.onfocusin=document.onfocusout=onchange;
  // All others:
  else window.onpageshow=window.onpagehide=window.onfocus=window.onblur=onchange;

  function onchange(evt){

    var v="visible",
      h="hidden",
      evtMap={focus:v,focusin:v,pageshow:v,blur:h,focusout:h,pagehide:h};

    document.body.classList.remove("hidden_");
    document.body.classList.remove("visible_");
    evt=evt||window.event;

    if(evt.type in evtMap)
      document.body.classList.add(evtMap[evt.type]+"_");
    else document.body.classList.add(this[hidden]?"hidden_":"visible_")

  }

  // set the initial state (but only if browser supports the Page Visibility API)
  if(document[hidden]!==undefined)
    onchange({type:document[hidden]?"blur":"focus"});

  // Detect if chat window is currently active END -----------------------------

})(jQuery,Drupal,drupalSettings);
